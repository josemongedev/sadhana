import StaticAuth from "components/Auth";
import { IOrder, IProduct } from "interfaces";
import { GetServerSideProps, GetServerSidePropsResult } from "next";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectOrderById } from "state/selectors/orders.selectors";
import { selectLoggedUserId } from "state/selectors/user.selectors";
import Loading from "../../components/Loading/index";
import * as OI from "../../components/Styles/orderId.styles";
import { getProductsList } from "../../services/products.requests";

interface IProductExtra {
  [key: string]: IProduct;
}

interface IOrderIdPageProps {
  orderId: string;
}

const OrderIdPage: React.FC<IOrderIdPageProps> = ({ orderId }) => {
  const router = useRouter();
  const orderIdSelected = useSelector(selectOrderById(orderId));
  const userId = useSelector(selectLoggedUserId) as string;

  const [productsList, setProductsList] = useState<IProduct[] | null>(null);
  const [orderDetail, setOrderDetail] = useState<IOrder | null>(null);

  // This will reload the cached orders with Redux
  useEffect(() => {
    const requestOrder = async () => {
      !orderIdSelected && router.push("/orders");
      setOrderDetail(orderIdSelected as IOrder);
      const idList = orderIdSelected?.products.map(
        (product) => product.productId
      );
      const { data: fetchedProducts } = await getProductsList(
        idList as string[]
      );
      setProductsList(fetchedProducts);
    };
    // Only run the first time, when it's not cached
    !orderDetail && requestOrder();
  }, [userId, orderIdSelected, router, orderDetail]);

  return (
    <StaticAuth>
      {orderDetail && productsList ? (
        <OI.Container>
          <OI.Wrapper>
            <OI.Title>ORDER #{orderDetail._id}</OI.Title>
            <OI.Top>
              <OI.TopButton>
                <Link href={"/orders"}>GO BACK TO ORDERS</Link>
              </OI.TopButton>
              <OI.TopTexts>
                <OI.TopText>
                  Orders products:({orderDetail.products.length})
                </OI.TopText>
              </OI.TopTexts>
              <OI.TopButton filled>
                <Link href={"/account"}>ACCOUNT SETTINGS</Link>
              </OI.TopButton>
            </OI.Top>
            <OI.Bottom>
              <OI.BottomHeader>
                <OI.HeaderItem>
                  <b>Shipping Address:&nbsp;</b> {orderDetail.address}
                </OI.HeaderItem>
                <OI.HeaderItem>
                  <b>Order Total:&nbsp;</b>${orderDetail.amount}
                </OI.HeaderItem>
                <OI.HeaderItem>
                  <b>Order status:&nbsp;</b>
                  {orderDetail.status}
                </OI.HeaderItem>
                <OI.HeaderItem>
                  <b>Date:&nbsp;</b>
                  {orderDetail.createdAt}
                </OI.HeaderItem>
              </OI.BottomHeader>
              <OI.BottomTitle>PRODUCTS LIST:</OI.BottomTitle>
              <OI.BottomProductsList>
                {productsList &&
                  orderDetail.products.map((product: IOrderItem, idx) => {
                    const extraData = productsList.find(
                      (p) => p._id === product.productId
                    );
                    return (
                      <OI.ProductRow key={idx + product.productId}>
                        <OI.ProductGeneral>
                          <OI.ProductItem>
                            <b>ID:</b>
                            {product.productId}
                          </OI.ProductItem>
                          <OI.ProductItem>
                            <b>Quantity:</b>
                            {product.quantity}
                          </OI.ProductItem>
                          <OI.ProductItem>
                            <b>Color:</b>
                            {product.color}
                          </OI.ProductItem>
                          <OI.ProductItem>
                            <b>Size:</b>
                            {product.size}
                          </OI.ProductItem>
                          <OI.ProductItem>
                            <b>Title:</b>
                            {extraData?.title ?? ""}
                          </OI.ProductItem>
                          <OI.ProductItem>
                            <b>Price:</b>
                            {extraData?.price ?? ""}
                          </OI.ProductItem>
                        </OI.ProductGeneral>
                        <Image
                          objectFit="scale-down"
                          height={200}
                          width={200}
                          src={extraData?.img || "/images/question-crop.jpg"}
                          alt={`Product ID ${product.productId}`}
                        />
                      </OI.ProductRow>
                    );
                  })}
              </OI.BottomProductsList>
            </OI.Bottom>
          </OI.Wrapper>
        </OI.Container>
      ) : (
        <Loading />
      )}
    </StaticAuth>
  );
};

export const getServerSideProps: GetServerSideProps = async ({
  params,
}): Promise<GetServerSidePropsResult<IOrderIdPageProps>> => {
  const orderId = params!.orderId as string;

  return {
    props: { orderId },
  };
};

export default OrderIdPage;
