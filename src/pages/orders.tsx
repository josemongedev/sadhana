import StaticAuth from "components/Auth";
import { IOrder } from "interfaces";
import * as OR from "components/Styles/orders.styles";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectLoggedUserId } from "state/selectors/user.selectors";
import { truncate } from "lodash";
import Link from "next/link";
import {
  selectOrdersState,
  selectRecentOrders,
} from "state/selectors/orders.selectors";
import { getOrdersDispatcher } from "state/async/orders.async";

const OrderProducts: React.FC<{ orderId: string; products: ICartItem[] }> = ({
  orderId,
  products,
}) => {
  return (
    <>
      {products.map((product) => (
        <OR.Product key={orderId + product.productId}>
          <OR.ProductId>
            <b>ID:</b> {product.productId}
          </OR.ProductId>
          {/* <OR.ProductColor>
            <b>Color:</b> {product.color}
          </OR.ProductColor>
          <OR.ProductSize>
            <b>Size:</b> {product.size}
          </OR.ProductSize> */}
          <OR.ProductAmount>
            <b>Quantity:</b> {product.quantity}
          </OR.ProductAmount>
        </OR.Product>
      ))}
    </>
  );
};
interface IOrderItem {
  order: IOrder;
}
const OrderItem: React.FC<IOrderItem> = ({ order }) => {
  return (
    <OR.Details>
      <OR.Summary>
        <OR.SummaryItem>
          <b>ID:&nbsp;</b>
          {truncate(order._id, { length: 20 })}
        </OR.SummaryItem>
        <OR.SummaryItem
          style={{
            color:
              order.status === "approved"
                ? "green"
                : order.status === "declined"
                ? "red"
                : "blue",
          }}
        >
          <b>Status:&nbsp;</b>
          {order.status}
        </OR.SummaryItem>

        <OR.SummaryItem>
          <b>Total:&nbsp;</b>${order.amount}
        </OR.SummaryItem>
      </OR.Summary>
      <OR.DetailsExtended>
        <span>
          <Link href={`/order/${order._id}`}>
            <i style={{ color: "blue" }}>See more...</i>
          </Link>
        </span>
        <br />
        <span>
          <b>Delivery Address:&nbsp;</b>
          {order.address}
        </span>
        {order.products && (
          <OrderProducts orderId={order._id} products={order.products} />
        )}
      </OR.DetailsExtended>
    </OR.Details>
  );
};
interface OrdersPageProps {}
const OrdersPage = (props: OrdersPageProps) => {
  const dispatch = useDispatch();
  const userId = useSelector(selectLoggedUserId) as string;
  const { isFetching, error } = useSelector(selectOrdersState);
  const orders = useSelector(selectRecentOrders);
  useEffect(() => {
    getOrdersDispatcher(userId, dispatch);
  }, [userId, dispatch]);
  return (
    <StaticAuth>
      {isFetching && <div>Loading orders...</div>}
      {!error && !isFetching && (
        <OR.Container>
          <OR.Wrapper>
            <OR.Title>YOUR ORDERS</OR.Title>
            <OR.Top>
              <Link href={"/"}>
                <OR.TopButton>CONTINUE SHOPPING</OR.TopButton>
              </Link>
              <OR.TopTexts>
                <OR.TopText>Total orders: ({orders.length})</OR.TopText>
              </OR.TopTexts>
              <Link href={"/account"}>
                <OR.TopButton filled>ACCOUNT SETTINGS</OR.TopButton>
              </Link>
            </OR.Top>
            <OR.Bottom>
              <OR.Info>
                {orders.length > 0
                  ? orders.map((order) => (
                      <OrderItem key={order._id} order={order} />
                    ))
                  : "No orders have been placed yet."}
              </OR.Info>
            </OR.Bottom>
          </OR.Wrapper>
        </OR.Container>
      )}
    </StaticAuth>
  );
};

export default OrdersPage;
