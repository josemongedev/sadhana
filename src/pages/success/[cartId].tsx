import StaticAuth from "components/Auth";
import useProcessLastOrder from "hooks/useProcessLastOrder";
import { GetServerSideProps, GetServerSidePropsResult } from "next";
import React from "react";
import * as SS from "../../components/Styles/success.styles";

interface ISuccessProps {
  cartId: string;
}

const SuccessPage: React.FC<ISuccessProps> = ({ cartId }) => {
  const { error, isFetching, lastOrder, validCartId, productsList } =
    useProcessLastOrder(cartId);

  return (
    <StaticAuth>
      {validCartId && lastOrder && !isFetching && !error && (
        <SS.Container>
          <SS.Wrapper>
            <SS.Title>YOUR ORDER HAS BEEN PLACED</SS.Title>
            <SS.Top>
              <SS.TopButton>CONTINUE SHOPPING</SS.TopButton>
              <SS.TopTexts>
                <SS.TopText>
                  Orders products:({lastOrder.products.length})
                </SS.TopText>
              </SS.TopTexts>
              <SS.TopButton filled>ACCOUNT SETTINGS</SS.TopButton>
            </SS.Top>
            <SS.Bottom>
              <SS.BottomHeader>
                <SS.HeaderItem>
                  <b>Shipping Address:&nbsp;</b> {lastOrder.address}
                </SS.HeaderItem>
                <SS.HeaderItem>
                  <b>Order Total:&nbsp;</b>${lastOrder.amount}
                </SS.HeaderItem>
                <SS.HeaderItem>
                  <b>Order status:&nbsp;</b>
                  {lastOrder.status}
                </SS.HeaderItem>
              </SS.BottomHeader>
              <SS.BottomTitle>PRODUCTS LIST:</SS.BottomTitle>
              <SS.BottomProductsList>
                {lastOrder.products.map((product: IOrderItem) => (
                  <SS.ProductRow key={product.productId}>
                    <SS.ProductGeneral>
                      <SS.ProductItem>
                        <b>ID:</b>
                        {product.productId}
                      </SS.ProductItem>
                      <SS.ProductItem>
                        <b>Quantity:</b>
                        {product.quantity}
                      </SS.ProductItem>
                      <SS.ProductItem>
                        <b>Color:</b>
                        {product.color}
                      </SS.ProductItem>
                      <SS.ProductItem>
                        <b>Size:</b>
                        {product.size}
                      </SS.ProductItem>

                      {productsList.map((pItem, plIdx) => {
                        if (pItem._id === product.productId)
                          return (
                            <React.Fragment key={plIdx}>
                              <SS.ProductItem>
                                <b>Title:</b>
                                {pItem.title}
                              </SS.ProductItem>
                              <SS.ProductItem>
                                <b>Price:</b>
                                {pItem.price}
                              </SS.ProductItem>
                            </React.Fragment>
                          );
                      })}
                    </SS.ProductGeneral>
                  </SS.ProductRow>
                ))}
              </SS.BottomProductsList>
            </SS.Bottom>
          </SS.Wrapper>
        </SS.Container>
      )}
    </StaticAuth>
  );
};

export const getServerSideProps: GetServerSideProps = async ({
  params,
}): Promise<GetServerSidePropsResult<ISuccessProps>> => {
  const cartId = params?.cartId;

  if (!cartId) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: { cartId: cartId as string },
  };
};

export default SuccessPage;
