import Image from "next/image";
import Link from "next/link";
import React from "react";
import * as P404 from "components/Styles/404.styles";

interface Props {}

const Page404 = (props: Props) => {
  return (
    <P404.Container>
      <P404.ImageLayout
        src="https://images.pexels.com/photos/1171386/pexels-photo-1171386.jpeg?cs=srgb&dl=pexels-tim-mossholder-1171386.jpg&fm=jpg"
        alt="Page not found"
      />
      <P404.Column>
        <P404.Title>
          The page you&apos;re looking for doesn&apos;t exist
        </P404.Title>
        <Link href="/">
          <P404.Anchor>Go back home</P404.Anchor>
        </Link>
      </P404.Column>
    </P404.Container>
  );
};

export default Page404;
