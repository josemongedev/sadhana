import React from "react";

interface Props {}

const Canceled = (props: Props) => {
  return <div>Payment canceled!</div>;
};

export default Canceled;
