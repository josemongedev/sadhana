import * as PL from "components/ProductList/ProductList.styles";
import Products from "components/Products/Products";
import { EColors, ESizes } from "constant";
import { IFilters, TSort } from "interfaces";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useState } from "react";

interface IProductsProps {}

const ProductsPage: NextPage<IProductsProps> = () => {
  const { query } = useRouter();
  const { category } = query;
  const [filters, setFilters] = useState<IFilters>({});
  const [sort, setSort] = useState<TSort>("newest");
  const handleFilters: React.ChangeEventHandler<HTMLSelectElement> = (e) => {
    const value = e.target.value;
    setFilters({
      ...filters,
      [e.target.name]: value,
    });
  };
  const clearFilters: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    setSort("newest");
    setFilters({});
  };
  return (
    <PL.Container>
      {category && <PL.Title>{(category as string)!.toUpperCase()}</PL.Title>}
      <PL.FilterContainer>
        <PL.Filter>
          <PL.FilterText>Filter Products:</PL.FilterText>
          <PL.Select name="color" onChange={handleFilters}>
            <PL.Option disabled selected={!filters["color"]}>
              Color
            </PL.Option>
            {Object.entries(EColors).map(([key, value]) => (
              <PL.Option key={key}>{value}</PL.Option>
            ))}
          </PL.Select>
          <PL.Select name="size" onChange={handleFilters}>
            <PL.Option disabled selected={!filters["size"]}>
              Size
            </PL.Option>
            {Object.entries(ESizes).map(([key, value]) => (
              <PL.Option key={key}>{value}</PL.Option>
            ))}
          </PL.Select>
        </PL.Filter>
        <PL.Filter>
          <PL.FilterText>Sort Products:</PL.FilterText>
          <PL.Select onChange={(e) => setSort(e.target.value as TSort)}>
            <PL.Option value="newest" selected={sort === "newest"}>
              Newest
            </PL.Option>
            <PL.Option value="asc">Price(asc)</PL.Option>
            <PL.Option value="desc">Price(desc)</PL.Option>
          </PL.Select>
        </PL.Filter>
        <PL.Filter>
          <PL.Button onClick={clearFilters}>Clear filters</PL.Button>
        </PL.Filter>
      </PL.FilterContainer>
      <Products
        opaque={false}
        cat={(category as string) || ""}
        filters={filters}
        sort={sort}
      />
    </PL.Container>
  );
};

export default ProductsPage;
