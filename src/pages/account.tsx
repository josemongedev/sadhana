import StaticAuth from "components/Auth";
import { IUser } from "interfaces";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { ChangeEventHandler, MouseEventHandler, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as AC from "components/Styles/account.styles";
import { fileUpload } from "utils/file.upload";
import {
  updateUserDispatcher,
  deleteUserDispatcher,
  logoutDispatcher,
} from "state/async/user.async";
import { selectCurrentUser, selectUser } from "state/selectors/user.selectors";

type TValidateUser = IUser & { repeat: string };

type AccountPageProps = {};
const AccountPage: NextPage<AccountPageProps> = ({}: AccountPageProps) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const currentUser = useSelector(selectCurrentUser);
  const { isFetching, error } = useSelector(selectUser);

  const [msg, setMsg] = useState({ color: "blue", text: "" });
  const [input, setInput] = useState<TValidateUser>({} as TValidateUser);

  // Form handlers
  const handleSubmit: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    const form = document.getElementById("form-user-update") as HTMLFormElement;
    form.checkValidity();
    if (form.reportValidity()) {
      if (input?.repeat === input?.password) {
        // Attempt update to API server
        updateUserDispatcher(dispatch, currentUser?._id as string, input);
        if (!error) {
          setMsg({ color: "green", text: "User update successfull!" });
        } else {
          setMsg({
            color: "red",
            text: "Something went wrong. Couldn't update user.",
          });
        }
      } else setMsg({ color: "red", text: "Passwords don't match" });
    } else {
      setMsg({
        color: "red",
        text: "Either incorrect input or required fields missing",
      });
    }
  };

  const handleDelete: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    deleteUserDispatcher(dispatch, currentUser?._id as string); // Delete user from DB
    logoutDispatcher(dispatch); // Clears server cookie
    router.push("/");
  };

  const handleInput: ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (e) => setInput({ ...input, [e.target.name]: e.target.value });

  const handleImageUpload: ChangeEventHandler<HTMLInputElement> = (e) => {
    e.preventDefault();
    const file = (e?.target?.files as FileList)[0] as File;
    if (file) {
      setMsg({ color: "blue", text: "Uploading file" });
      fileUpload(
        file,
        (downloadURL) => {
          setInput({ ...input, img: downloadURL });
        },
        (text, color) => {
          setMsg({ color, text });
        }
      );
    } else {
      setMsg({ color: "red", text: "Please select a valid image file" });
    }
  };

  return (
    <StaticAuth>
      <AC.Container>
        {
          <AC.Wrapper>
            <AC.Title>PROFILE INFORMATION</AC.Title>
            <AC.Top>
              <AC.Info>Here you can update your profile information</AC.Info>
            </AC.Top>
            <form id="form-user-update" onSubmit={(e) => e.preventDefault()}>
              <AC.Form>
                <AC.Row style={{ justifyContent: "center" }}>
                  {!!msg.text && (
                    <AC.Error messageColor={msg.color}>
                      <b>{msg.text}</b>
                    </AC.Error>
                  )}
                </AC.Row>
                <AC.Row>
                  <AC.Column style={{ flex: 2 }}>
                    <span style={{ marginBottom: "2%" }}>Profile picture</span>
                    <AC.Column>
                      <AC.Image
                        src={currentUser?.img || "/images/question-crop.jpg"}
                        alt={`Profile picture for ${currentUser?.username}`}
                      />
                      <AC.FileLabel htmlFor={"fileImg"}>
                        Upload photo
                      </AC.FileLabel>
                      <AC.File
                        style={{ visibility: "hidden" }}
                        id="fileImg"
                        accept=".jpg, .jpeg, .png"
                        type="file"
                        onChange={handleImageUpload}
                      />
                    </AC.Column>
                  </AC.Column>
                  <AC.Column style={{ flex: 1 }}>
                    <AC.Label htmlFor={"username"}>Username</AC.Label>
                    <AC.Input
                      name="username"
                      placeholder={currentUser?.username}
                      onChange={handleInput}
                      pattern="[A-Za-z0-9]{0,15}"
                      disabled
                    />
                    <AC.Label htmlFor="password">Password</AC.Label>
                    <AC.Input
                      name="password"
                      placeholder="************"
                      onChange={handleInput}
                      pattern="[A-Za-z0-9]{4,20}"
                    />
                    <AC.Label htmlFor="repeat">Repeat Password</AC.Label>
                    <AC.Input
                      name="repeat"
                      placeholder="************"
                      onChange={handleInput}
                      pattern="[A-Za-z0-9]{4,20}"
                    />
                    <AC.Label htmlFor="phone">Phone</AC.Label>
                    <AC.Input
                      name="phone"
                      placeholder={currentUser?.phone}
                      onChange={handleInput}
                      pattern="[0-9]{10,20}"
                    />
                  </AC.Column>
                  <AC.Column style={{ flex: 2 }}>
                    <AC.Label htmlFor="email">Email</AC.Label>
                    <AC.Input
                      name="email"
                      placeholder={currentUser?.email}
                      onChange={handleInput}
                      pattern="[^@ \t\r\n]+@[^@ \t\r\n]+.[^@ \t\r\n]{0,30}"
                    />
                    <AC.Label htmlFor="fullname">Fullname</AC.Label>
                    <AC.Input
                      name="fullname"
                      placeholder={currentUser?.fullname}
                      onChange={handleInput}
                      pattern="[A-Za-z ]{0,20}"
                    />
                    <AC.Label htmlFor="address">Address</AC.Label>
                    <AC.Textarea
                      rows={6}
                      name="address"
                      placeholder={currentUser?.address}
                      onChange={handleInput}
                      maxLength={200}
                    />
                  </AC.Column>
                </AC.Row>

                <AC.Row style={{ flex: 1, justifyContent: "center" }}>
                  <AC.Button onClick={(e) => router.push("/orders")}>
                    VIEW ORDERS
                  </AC.Button>
                  <AC.Button
                    onClick={(e) => handleSubmit(e)}
                    disabled={isFetching}
                  >
                    CONFIRM CHANGES
                  </AC.Button>

                  <AC.Button
                    onClick={(e) =>
                      window.confirm(
                        "Are you sure to delete this user account? This step is irreversible."
                      ) && handleDelete(e)
                    }
                    disabled={isFetching}
                  >
                    DELETE ACCOUNT
                  </AC.Button>
                </AC.Row>
              </AC.Form>
            </form>
          </AC.Wrapper>
        }
      </AC.Container>
    </StaticAuth>
  );
};

export default AccountPage;
