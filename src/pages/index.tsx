import Categories from "components/Categories/Categories";
import HeroSlider from "components/HeroSlider/HeroSlider";
import HeroMain from "components/HeroMain/HeroMain";
import ProductsSlider from "components/ProductsSlider/ProductsSlider";
import type { GetServerSideProps, NextPage } from "next";

interface IHomePageProps {
  // cookie: any;
}

const HomePage: NextPage<IHomePageProps> = (props) => {
  return (
    <>
      <HeroMain />
      <HeroSlider />
      <ProductsSlider />
      <Categories />
    </>
  );
};

// export const getServerSideProps: GetServerSideProps<IHomePageProps> = async (
//   context
// ) => {
//   // const cookie = context?.req?.headers?.cookie;

//   return {
//     props: {
//       // cookie,
//     },
//   };
// };
export default HomePage;
