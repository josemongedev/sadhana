import { Add, Remove } from "@mui/icons-material";
import RemoveShoppingCartIcon from "@mui/icons-material/RemoveShoppingCart";
import { updateCartDispatcher } from "state/async/cart.async";
import { selectCart } from "state/selectors/cart.selectors";
import StaticAuth from "components/Auth";
import * as CS from "components/Styles/cart.styles";

import { useRouter } from "next/router";
import React, { MouseEventHandler } from "react";
import { useDispatch, useSelector } from "react-redux";
import { authRequest } from "../services/requests.config";
interface Props {}

const CartPage = (props: Props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const cart = useSelector(selectCart);
  const { productsData } = cart;

  const handleRemoveItem = (pid: string) => {
    const cartItems = cart.cartItems.filter((itm) => itm.productId !== pid);
    const cartId = cart!.cartId as string;
    updateCartDispatcher(dispatch, cartId, cartItems, []);
  };

  const handleQuantityChange = (pid: string, quantity: number) => {
    const cartItems = cart.cartItems.map((itm) =>
      itm.productId === pid ? { ...itm, quantity } : itm
    );
    const cartId = cart!.cartId as string;
    updateCartDispatcher(dispatch, cartId, cartItems, []);
  };
  const handleCheckout: MouseEventHandler<HTMLButtonElement> = async (e) => {
    e.preventDefault();
    const res = await authRequest.post(
      `${process.env.SADHANA_API_BACKEND}/checkout/create-checkout-session`
    );
    if (res.status === 201) router.push(res.data.url);
  };
  return (
    <StaticAuth>
      <CS.Container>
        <CS.Wrapper>
          <CS.Title>YOUR CART</CS.Title>
          <CS.Top>
            <CS.TopButton onClick={() => router.push("/")}>
              CONTINUE SHOPPING
            </CS.TopButton>
            <CS.TopTexts>
              <CS.TopText>Shopping cart({cart.quantity})</CS.TopText>
              <CS.TopText>Your Wishlist(0)</CS.TopText>
            </CS.TopTexts>

            <CS.TopButton
              onClick={handleCheckout}
              filled
              disabled={!cart.cartItems.length}
            >
              CHECKOUT NOW
            </CS.TopButton>
          </CS.Top>
          <CS.Bottom>
            <CS.Info>
              {cart.cartItems.map((item, idx) => (
                <CS.Product key={idx + item.productId}>
                  <CS.ProductDetail>
                    <CS.Image
                      src={productsData[item.productId]?.img}
                      alt={productsData[item.productId]?.title}
                    />
                    <CS.Details>
                      <CS.ProductName>
                        <b>Product:</b>
                        {productsData[item.productId]?.title}
                      </CS.ProductName>
                      <CS.ProductId>
                        <b>ID:</b> {item.productId}
                      </CS.ProductId>
                      <CS.ProductColor color={item.color} />
                      <CS.ProductSize>
                        <b>Size:</b> {item.size}
                      </CS.ProductSize>
                    </CS.Details>
                  </CS.ProductDetail>
                  <CS.PriceDetail>
                    <CS.ProductAmountContainer>
                      <Add
                        style={{ cursor: "pointer" }}
                        onClick={(e) =>
                          handleQuantityChange(
                            item.productId,
                            item.quantity + 1
                          )
                        }
                      />
                      <CS.ProductAmount>{item.quantity}</CS.ProductAmount>
                      <Remove
                        style={{ cursor: "pointer" }}
                        onClick={(e) =>
                          item.quantity > 1 &&
                          handleQuantityChange(
                            item.productId,
                            item.quantity - 1
                          )
                        }
                      />
                    </CS.ProductAmountContainer>
                    <CS.ProductPrice>
                      {productsData[item.productId]!.price * item.quantity}
                    </CS.ProductPrice>
                    <CS.ProductOptions>
                      <RemoveShoppingCartIcon
                        style={{ cursor: "pointer" }}
                        onClick={(e) => handleRemoveItem(item.productId)}
                      />
                    </CS.ProductOptions>
                  </CS.PriceDetail>
                </CS.Product>
              ))}
            </CS.Info>
            <CS.Summary>
              <CS.SummaryTitle>ORDER SUMMARY</CS.SummaryTitle>
              <CS.SummaryItem>
                <CS.SummaryItemText>Subtotal</CS.SummaryItemText>
                <CS.SummaryItemPrice>
                  $ {cart.total.toFixed(2)}
                </CS.SummaryItemPrice>
              </CS.SummaryItem>
              <CS.SummaryItem>
                <CS.SummaryItemText>Estimated Shipping</CS.SummaryItemText>
                <CS.SummaryItemPrice>$ 5.90</CS.SummaryItemPrice>
              </CS.SummaryItem>
              <CS.SummaryItem>
                <CS.SummaryItemText>Shipping</CS.SummaryItemText>
                <CS.SummaryItemPrice>$ -5.90</CS.SummaryItemPrice>
              </CS.SummaryItem>
              <CS.SummaryItem total>
                <CS.SummaryItemText>Total</CS.SummaryItemText>
                <CS.SummaryItemPrice>
                  $ {cart.total.toFixed(2)}
                </CS.SummaryItemPrice>
              </CS.SummaryItem>

              <CS.SummaryButton
                onClick={handleCheckout}
                disabled={!cart.cartItems.length}
              >
                CHECKOUT NOW
              </CS.SummaryButton>
            </CS.Summary>
          </CS.Bottom>
        </CS.Wrapper>
      </CS.Container>
    </StaticAuth>
  );
};

export default CartPage;
