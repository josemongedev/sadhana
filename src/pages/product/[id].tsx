import { Add, Remove } from "@mui/icons-material";
import { IProduct } from "interfaces/products";
import {
  GetStaticPaths,
  GetStaticProps,
  GetStaticPropsResult,
  NextPage,
} from "next";
import { FormEvent } from "react";
import { getProductById, getProducts } from "services";
import * as PC from "../../components/Styles/product.styles";
import useAddToCart from "../../hooks/useAddToCart";

interface IProductProps {
  product: IProduct;
}

const ProductPage: NextPage<IProductProps> = ({ product }) => {
  // Hooks
  const { newCartItem, setNewCartItem, addToCart } = useAddToCart(product);

  // Interactivity handlers
  const handleSizeChange = (e: FormEvent<HTMLOptionElement>) =>
    setNewCartItem({
      ...newCartItem,
      size: (e.target as HTMLOptionElement).value,
    });

  const handleColorChange = (c: string) =>
    setNewCartItem({
      ...newCartItem,
      color: c,
    });

  const handleQuantityChange = (quantity: number) => {
    setNewCartItem({
      ...newCartItem,
      quantity,
    });
  };

  return (
    <PC.Container>
      <PC.Wrapper>
        <PC.ImgContainer>
          <PC.Image src={product.img} />
        </PC.ImgContainer>
        <PC.InfoContainer>
          <PC.Title>{product.title}</PC.Title>
          <PC.Desc>{product.desc}</PC.Desc>
          <PC.Price>$ {product.price}</PC.Price>
          <PC.FilterContainer>
            <PC.Filter>
              <PC.FilterTitle>Color</PC.FilterTitle>
              {product.color.map((c: string) => (
                <PC.FilterColor
                  selected={c === newCartItem.color}
                  color={c}
                  key={c}
                  onClick={(e) => handleColorChange(c)}
                />
              ))}
            </PC.Filter>
            <PC.Filter>
              <PC.FilterTitle>Size</PC.FilterTitle>
              <PC.FilterSize>
                {product.size.map((s) => (
                  <PC.FilterSizeOption key={s} onChange={handleSizeChange}>
                    {s}
                  </PC.FilterSizeOption>
                ))}
              </PC.FilterSize>
            </PC.Filter>
          </PC.FilterContainer>
          <PC.AddContainer>
            <PC.AmountContainer>
              <Remove
                style={{ cursor: "pointer" }}
                onClick={(e) =>
                  newCartItem.quantity > 1 &&
                  handleQuantityChange(newCartItem.quantity - 1)
                }
              />
              <PC.Amount>{newCartItem.quantity}</PC.Amount>
              <Add
                style={{ cursor: "pointer" }}
                onClick={(e) => handleQuantityChange(newCartItem.quantity + 1)}
              />
            </PC.AmountContainer>
            <PC.Button onClick={addToCart}>ADD TO CART</PC.Button>
          </PC.AddContainer>
        </PC.InfoContainer>
      </PC.Wrapper>
    </PC.Container>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const res = await getProducts();
  return {
    paths: res.data.map((product: IProduct) => `/product/${product._id}`),
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async ({
  params,
}): Promise<GetStaticPropsResult<IProductProps>> => {
  const res = await getProductById(params!.id as string);
  return {
    props: { product: res.data },
    // revalidate: 60 * 60 * 24, // 1 day in seconds
  };
};

export default ProductPage;
