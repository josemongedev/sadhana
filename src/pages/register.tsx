import { registerUserDispatcher } from "state/async/user.async";
import * as Register from "components/Styles/register.styles";
import { IUser } from "interfaces";
import React, {
  ChangeEventHandler,
  KeyboardEventHandler,
  MouseEventHandler,
  useState,
} from "react";
import { useDispatch } from "react-redux";
import { postUserExists } from "services";
import useDetectLogin from "../hooks/useDetectLogin";

type TValidateUser = IUser & { repeat: string };

interface Props {}

const RegisterPage = (props: Props) => {
  const dispatch = useDispatch();

  const [msg, setMsg] = useState({ color: "blue", text: "" });
  const [input, setInput] = useState<TValidateUser>({} as TValidateUser);
  const { error } = useDetectLogin();

  // Form handlers
  const usernameIsValid = async (username: string) => {
    if (username?.trim() !== "") {
      let inUse = false;
      try {
        const { data } = await postUserExists(username);
        inUse = data.inuse;
        !inUse && setInput({ ...input, username });
      } catch (e) {
        setMsg({ color: "red", text: "Error contacting the server" });
        return true;
      }
      if (inUse) {
        setMsg({ color: "red", text: "Username is already in use" });
        return true;
      }
    } else return true;
    return false;
  };

  const handleRegister = async () => {
    const form = document.getElementById(
      "form-user-register"
    ) as HTMLFormElement;
    form.checkValidity();
    if (form.reportValidity()) {
      const inUse = await usernameIsValid(input.username as string);
      if (!inUse) {
        if (input?.repeat === input?.password) {
          // Attempt update to API server
          registerUserDispatcher(input, dispatch);
          if (!error) {
            setMsg({ color: "green", text: "User registration successfull!" });
          } else {
            console.log(error);
          }
        } else setMsg({ color: "red", text: "Passwords don't match" });
      }
    } else {
      setMsg({
        color: "red",
        text: "Either incorrect input or required fields missing",
      });
    }
  };

  const handleInput: ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (e) => setInput({ ...input, [e.target.name]: e.target.value });

  const handleRegisterKey: KeyboardEventHandler<HTMLButtonElement> = async (
    e
  ) => {
    e.preventDefault();
    await handleRegister();
  };
  const handleRegisterMouse: MouseEventHandler<HTMLButtonElement> = async (
    e
  ) => {
    e.preventDefault();
    await handleRegister();
  };

  return (
    <Register.Container>
      <Register.Wrapper>
        <Register.Title>CREATE AN ACCOUNT</Register.Title>
        <Register.Form id="form-user-register">
          <Register.Input
            placeholder="username"
            name="username"
            onChange={handleInput}
            required
          />
          <Register.Input
            placeholder="fullname"
            name="fullname"
            onChange={handleInput}
          />
          <Register.Input
            type="textarea"
            placeholder="address"
            name="address"
            onChange={handleInput}
          />
          <Register.Input
            placeholder="phone"
            name="phone"
            onChange={handleInput}
          />
          <Register.Input
            placeholder="email"
            name="email"
            onChange={handleInput}
            required
          />
          <Register.Input
            placeholder="password"
            name="password"
            onChange={handleInput}
            required
          />
          <Register.Input
            placeholder="confirm password"
            name="repeat"
            onChange={handleInput}
            required
          />
          <Register.Agreement>
            By creating an account, I consent to the processing of my personal
            data in accordance with the <b>PRIVACY POLICY</b>
          </Register.Agreement>
          <Register.Button
            onClick={handleRegisterMouse}
            onKeyDown={handleRegisterKey}
          >
            CREATE ACCOUNT
          </Register.Button>
          {!!msg.text && (
            <Register.Error color={msg.color}>{msg.text}</Register.Error>
          )}
        </Register.Form>
      </Register.Wrapper>
    </Register.Container>
  );
};

export default RegisterPage;
