import { loginDispatcher } from "state/async/user.async";
import * as Login from "components/Styles/login.styles";
import useDetectLogin from "hooks/useDetectLogin";
import { NextPage } from "next";
import Link from "next/link";
import {
  KeyboardEventHandler,
  MouseEventHandler,
  useEffect,
  useState,
} from "react";
import { useDispatch } from "react-redux";

interface ILoginPageProps {}
const LoginPage: NextPage<ILoginPageProps> = (props: ILoginPageProps) => {
  const dispatch = useDispatch();
  const { isFetching, error, currentUser } = useDetectLogin();

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [dataSent, setDataSent] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>("");

  const handleLogin = () => {
    if (password && username) {
      loginDispatcher(dispatch, { username, password }); // Attempt login to API server
      setDataSent(true);
    }
    if (!password || !username) {
      const emptyErrMsg = (field: string) =>
        `${field} missing. Please enter the right credentials.`;
      setErrorMessage(emptyErrMsg(!username ? "Username" : "Password"));
    }
  };
  const handleLoginKey: KeyboardEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    handleLogin();
  };
  const handleLoginMouse: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    handleLogin();
  };

  useEffect(() => {
    if (dataSent) {
      if (error) setErrorMessage("Either username or password are incorrect.");
    }
  }, [error, dataSent]);

  return (
    <Login.Container>
      <Login.Wrapper>
        <Login.Title>SIGN IN</Login.Title>
        <Login.Form>
          <Login.Input
            placeholder="username"
            onChange={(e) => setUsername(e.target.value)}
          />
          <Login.Input
            type={"password"}
            placeholder="password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <Login.Button
            onKeyDown={handleLoginKey}
            onClick={handleLoginMouse}
            disabled={isFetching}
          >
            LOGIN
          </Login.Button>
          {errorMessage && <Login.Error>{errorMessage}</Login.Error>}
          {/* <Login.Link>Forgot Password?</Login.Link> */}
          <Link href={"/register"}>
            <Login.LinkInner>Create a new account</Login.LinkInner>
          </Link>
        </Login.Form>
      </Login.Wrapper>
    </Login.Container>
  );
};

export default LoginPage;
