import { IUser } from "../interfaces/user";
import { authRequest } from "./requests.config";

export const postUserExists = (username: string) =>
  authRequest.post(
    `${process.env.SADHANA_API_BACKEND}/auth/username`,
    { username },
    { withCredentials: true }
  );

export const postUserLogin = (user: Pick<IUser, "password" | "username">) =>
  authRequest.post(`${process.env.SADHANA_API_BACKEND}/auth/login`, user, {
    withCredentials: true,
  });

export const logoutUser = () =>
  authRequest.post(`${process.env.SADHANA_API_BACKEND}/auth/logout`, {
    withCredentials: true,
  });

export const getCheckUserStatus = () =>
  authRequest.get(`${process.env.SADHANA_API_BACKEND}/auth/check`, {
    withCredentials: true,
  });

export const registerUser = (user: Partial<IUser>) =>
  authRequest.post<IUser>(
    `${process.env.SADHANA_API_BACKEND}/auth/register`,
    user,
    {
      withCredentials: true,
    }
  );
