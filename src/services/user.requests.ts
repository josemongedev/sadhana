
import { IUser } from "interfaces";
import { authRequest } from "./requests.config";

export const getUserById = (id: string) =>
  authRequest.get<IUser>(`${process.env.SADHANA_API_BACKEND}/users/find/` + id, {
    withCredentials: true,
  });

export const deleteUserById = (id: string) =>
  authRequest.delete<string>(`${process.env.SADHANA_API_BACKEND}/users/` + id, {
    withCredentials: true,
  });

export const updateUserById = (id: string, user: Partial<IUser>) =>
  authRequest.put<IUser>(`${process.env.SADHANA_API_BACKEND}/users/` + id, user, {
    withCredentials: true,
  });
