
import { IOrder } from "interfaces";
import { authRequest } from "./requests.config";

export const getOrderById = (id: string) =>
  authRequest.get<IOrder>(`${process.env.SADHANA_API_BACKEND}/orders/find/` + id, {
    withCredentials: true,
  });

export const getOrdersByUserId = (userId: string) =>
  authRequest.get<IOrder[]>(`${process.env.SADHANA_API_BACKEND}/orders/user/${userId}`, {
    withCredentials: true,
  });
