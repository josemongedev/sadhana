import axios, { AxiosRequestConfig } from "axios";
//
import { wrapper } from "axios-cookiejar-support";
import { CookieJar } from "tough-cookie";

// const baseConfig: AxiosRequestConfig<any> = {
//   baseURL: process.env.SADHANA_API_BACKEND,
// };

const jar = new CookieJar();
const axiosConfig: AxiosRequestConfig = {
  withCredentials: true,
  headers: {
    "Access-Control-Allow-Credentials": true,
    "Access-Control-Allow-Origin": process.env.FRONTEND_URL,
  },
  jar,
};

// export const publicRequest = axios.create(baseConfig);

export const authRequest = wrapper(axios.create(axiosConfig));
