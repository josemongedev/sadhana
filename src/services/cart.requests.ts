
import { authRequest } from "./requests.config";

export interface IResponseCart {
  _id: string;
  products: (ICartItem & { _id: string })[];
}

export const postCheckout = () =>
  authRequest.post(`${process.env.SADHANA_API_BACKEND}/checkout/create-checkout-session`, {
    withCredentials: true,
  });

export const postProductToCart = (product: ICartItem, id: string) =>
  authRequest.post<object & IResponseCart>(
    `${process.env.SADHANA_API_BACKEND}/carts/`,
    {
      userId: id,
      products: [product],
    },
    {
      withCredentials: true,
    }
  );

export const putCartWithProducts = (
  products: Partial<ICartItem>[],
  id: string
) =>
  authRequest.put<object & IResponseCart>(
    `${process.env.SADHANA_API_BACKEND}/carts/${id}`,
    {
      products,
    },
    {
      withCredentials: true,
    }
  );

export const getCartByUserId = (userId: string) =>
  authRequest.get<object & IResponseCart>(
    `${process.env.SADHANA_API_BACKEND}/carts/find/${userId}`,
    {
      withCredentials: true,
    }
  );

export const getCartById = (cartId: string) =>
  authRequest.get<object & IResponseCart>(`${process.env.SADHANA_API_BACKEND}/carts/${cartId}`, {
    withCredentials: true,
  });

export const deleteCart = (id: string) =>
  authRequest.delete<string>(`${process.env.SADHANA_API_BACKEND}/carts/${id}`, {
    withCredentials: true,
  });
