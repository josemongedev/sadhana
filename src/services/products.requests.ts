import { IProduct } from "interfaces/products";
import { authRequest } from "./requests.config";

export const getProductById = (id: string) =>
  authRequest.get<IProduct>(
    `${process.env.SADHANA_API_BACKEND}/products/find/` + id
  );

export const getProducts = (cat?: string | undefined) =>
  authRequest.get<IProduct[]>(
    `${process.env.SADHANA_API_BACKEND}/products${
      cat ? `?category=${cat}` : ""
    }`
  );

export const getProductsList = (list: string[]) =>
  authRequest.post<IProduct[]>(
    `${process.env.SADHANA_API_BACKEND}/products/list`,
    { list }
  );
