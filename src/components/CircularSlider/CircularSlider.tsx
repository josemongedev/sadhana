import { ArrowLeftOutlined, ArrowRightOutlined } from "@mui/icons-material";
import * as CS from "components/CircularSlider/CircularSlider.styles";
import React, { useEffect, useState } from "react";

interface CircularSliderProps {
  slides: JSX.Element[];
  heading: string;
}

const CircularSlider: React.FC<CircularSliderProps> = ({ slides, heading }) => {
  const [collection, setCollection] = useState<JSX.Element[]>([]);
  const [slideIndex, setSlideIndex] = useState(0);
  const handleClick = (direction: "left" | "right") => {
    const newCollect: any[] = collection.slice(); //Shallow copy
    if (direction === "left") {
      setSlideIndex(slideIndex - 1);
      newCollect.unshift(newCollect.pop());
    } else {
      setSlideIndex(slideIndex + 1);
      newCollect.push(newCollect.shift());
    }
    setCollection(newCollect);
  };
  useEffect(() => {
    setCollection(slides);
    setSlideIndex(Math.ceil(slides.length / 2));
  }, [slides]);
  return (
    <CS.Section>
      <CS.Heading>{heading}</CS.Heading>
      <CS.Container>
        <CS.Arrow $direction="left" onClick={() => handleClick("left")}>
          <ArrowLeftOutlined />
        </CS.Arrow>
        <CS.AnimWrapper $slideindex={slideIndex - 4}>
          <CS.Wrapper $slideindex={slideIndex}>{collection}</CS.Wrapper>
        </CS.AnimWrapper>
        <CS.Arrow $direction="right" onClick={() => handleClick("right")}>
          <ArrowRightOutlined />
        </CS.Arrow>
      </CS.Container>
    </CS.Section>
  );
};

export default CircularSlider;
