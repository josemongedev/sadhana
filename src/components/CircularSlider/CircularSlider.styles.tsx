import styled from "styled-components";
import { mobile, tablet } from "responsive";

interface ISection {}
export const Section = styled.section<ISection>`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 20px;
  justify-content: center;
  align-items: center;
  ${mobile({ padding: "0px" })}
`;
interface IHeading {}
export const Heading = styled.h1<IHeading>`
  font-size: 3rem;
`;

export enum ArrowDirection {
  LEFT = "left",
  RIGHT = "right",
}
export type ArrowDirectionType = ArrowDirection.LEFT | ArrowDirection.RIGHT;

export const Container = styled.div`
  width: 100%;
  /* height: 100vh; */
  display: flex;
  position: relative;
  overflow: hidden;
`;

interface IArrow {
  $direction: string;
}
export const Arrow = styled.div<IArrow>`
  width: 50px;
  height: 50px;
  color: white;
  background-color: #0f0202;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  bottom: 0;
  left: ${(props) =>
    props.$direction === ArrowDirection.LEFT ? "10px" : "auto"};
  right: ${(props) =>
    props.$direction === ArrowDirection.RIGHT ? "10px" : "auto"};
  margin: auto;
  cursor: pointer;
  opacity: 0.5;
  z-index: 2;
`;

enum EElementSize {
  FOUR_SLIDES = 25,
  TWO_SLIDES = 50,
  ONE_SLIDE = 100,
}

interface IWrapper {
  $slideindex: number;
}
export const Wrapper = styled.div<IWrapper>`
  height: 100%;
  display: flex;
  flex-direction: row;
  transition: all 1.5s ease;
  transform: translateX(${(props) => props.$slideindex * -25}vw);
`;
interface IAnimWrapper {
  $slideindex: number;
}
export const AnimWrapper = styled.div<IAnimWrapper>`
  height: 100%;
  display: flex;
  flex-direction: row;
  position: relative;
  transform: translateX(${(props) => props.$slideindex * 25}vw);
`;

interface ISlide {
  $bg: string;
}
export const Slide = styled.div<ISlide>`
  width: ${EElementSize.FOUR_SLIDES}vw;
  ${tablet({ width: `${EElementSize.TWO_SLIDES}vw` })};
  ${mobile({ width: `${EElementSize.ONE_SLIDE}vw` })};
  display: flex;
  align-items: center;
  /* background-color: #${(props) => props.$bg}; */
`;

export const ImgContainer = styled.div`
  height: 100%;
  flex: 1;
`;

export const Image = styled.img`
  height: 80%;
`;

export const InfoContainer = styled.div`
  flex: 1;
  padding: 50px;
`;

export const Title = styled.h1`
  font-size: 70px;
`;

export const Desc = styled.p`
  margin: 50px 0;
  font-size: 20px;
  font-weight: 500;
  letter-spacing: 3px;
`;

export const Button = styled.button`
  padding: 10px;
  font-size: 20px;
  background-color: transparent;
  cursor: pointer;
`;
