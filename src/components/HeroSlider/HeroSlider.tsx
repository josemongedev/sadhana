import { CollectionItem, sliderItems } from "constant";
import React from "react";
import Slider from "../Slider/Slider";
import {
  Button,
  Desc,
  Image,
  ImgContainer,
  InfoContainer,
  Slide,
  Title,
} from "../Slider/Slider.styles";

interface Props {}

const HeroBanner = (props: Props) => {
  return (
    <Slider sizeSlides={sliderItems.length}>
      {sliderItems.map((item: CollectionItem) => (
        <Slide key={item.id} $bg={item.bg}>
          <ImgContainer>
            <Image src={item.img} alt={item.desc} />
          </ImgContainer>
          <InfoContainer>
            <Title>{item.title}</Title>
            <Desc>{item.desc}</Desc>
            <Button>SHOW NOW</Button>
          </InfoContainer>
        </Slide>
      ))}
    </Slider>
  );
};

export default HeroBanner;
