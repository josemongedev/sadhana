import Link from "next/link";
import React from "react";
import { Anchor, Container, Image, Info, Title } from "./CategoryItem.styles";

interface CategoryItemProps {
  item: any;
}

const CategoryItem: React.FC<CategoryItemProps> = ({ item }) => {
  return (
    <Container>
      <Image src={item.img} alt={item.title} />
      <Info>
        <Title>{item.title}</Title>
        <Link href={`/products?category=${item.cat}`}>
          <Anchor>SHOP NOW</Anchor>
        </Link>
      </Info>
    </Container>
  );
};

export default CategoryItem;
