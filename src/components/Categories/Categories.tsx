import React from "react";
import styled from "styled-components";
import { categories } from "constant";
import { mobile } from "responsive";
import CategoryItem from "./CategoryItem";

const Container = styled.div``;

export const Row = styled.div`
  display: flex;
  padding: 20px;
  justify-content: space-between;
  ${mobile({ padding: "0px", flexDirection: "column" })}
`;
interface IHeading {}
export const Heading = styled.h1<IHeading>`
  font-size: 3rem;
`;
interface CategoriesProps {}
const Categories = (props: CategoriesProps) => {
  const categories1 = categories.slice(0, categories.length / 2);
  const categories2 = categories.slice(
    categories.length / 2,
    categories.length
  );
  return (
    <Container>
      <Row style={{ justifyContent: "center" }}>
        <Heading>Categories</Heading>
      </Row>

      <Row>
        {categories1.map((item) => (
          <CategoryItem key={item.id} item={item} />
        ))}
      </Row>
      <Row>
        {categories2.map((item) => (
          <CategoryItem key={item.id} item={item} />
        ))}
      </Row>
    </Container>
  );
};

export default Categories;
