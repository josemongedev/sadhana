import {
  Facebook,
  Instagram,
  MailOutline,
  Phone,
  Pinterest,
  Room,
  Twitter,
} from "@mui/icons-material";
import React from "react";
import { usefulLinks } from "constant";
import * as FS from "./Footer.styles";
import Newsletter from "../../Newsletter/Newsletter";

interface Props {}
const Footer = (props: Props) => {
  return (
    <FS.Container>
      <FS.Left>
        <FS.Logo>Sadhana</FS.Logo>
        <FS.Desc>
          Suspendisse euismod eu quam et lobortis. Donec sit amet lectus sapien.
          Donec justo metus, congue non ullamcorper sed, euismod eget sem.
          Aenean placerat sollicitudin lacinia. In elementum semper justo, non
          hendrerit mauris. Duis iaculis interdum justo, a sollicitudin lorem
          feugiat sed. Fusce commodo justo consequat, placerat ex sed, dapibus
          eros. Sed pharetra, turpis condimentum euismod dapibus, ex neque
          lacinia dolor, quis blandit est elit sit amet odio. Suspendisse
          finibus ut purus sodales eleifend. Mauris a vulputate nibh, vitae
          viverra dui. Vestibulum porta condimentum elementum. Maecenas eu porta
          dui, eget congue erat.
        </FS.Desc>
      </FS.Left>
      <FS.Center>
        <FS.Title>Newsletter</FS.Title>
        <Newsletter />
        <FS.Title>Contact</FS.Title>
        <FS.ContactItem>
          <Room style={{ marginRight: "10px" }} />
          45211 NY, USA
        </FS.ContactItem>
        <FS.ContactItem>
          <Phone style={{ marginRight: "10px" }} />
          +1 234 567 891
        </FS.ContactItem>
        <FS.ContactItem>
          <MailOutline style={{ marginRight: "10px" }} />
          info@sadhana.com
        </FS.ContactItem>
        <FS.SocialContainer>
          <FS.SocialIcon color="3B5999">
            <Facebook />
          </FS.SocialIcon>
          <FS.SocialIcon color="E4405F">
            <Instagram />
          </FS.SocialIcon>
          <FS.SocialIcon color="55ACEE">
            <Twitter />
          </FS.SocialIcon>
          <FS.SocialIcon color="E60023">
            <Pinterest />
          </FS.SocialIcon>
        </FS.SocialContainer>
      </FS.Center>
      <FS.Right>
        <FS.Title>Useful Links</FS.Title>
        <FS.List>
          {usefulLinks.map((link) => (
            <FS.ListItem key={link.name}>{link.name}</FS.ListItem>
          ))}
        </FS.List>
      </FS.Right>
    </FS.Container>
  );
};

export default Footer;
