import styled from "styled-components";

const Container = styled.div`
  height: 30px;
  background-color: #292a2b;
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
`;

const Announcement = () => {
  return (
    <Container>
      <span>SUPER FAST SHIPPING! 35% DISCOUNT FOR ORDERS OVER $40</span>
    </Container>
  );
};

export default Announcement;
