// components/layout.js
import Navbar from "./Navbar/Navbar";
import Footer from "./Footer/Footer";
import Announcement from "./Announcement/Announcement";
import Newsletter from "../Newsletter/Newsletter";
import { PropsWithChildren } from "react";

export const Layout: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <>
      <Announcement />
      <Navbar />
      <main>{children}</main>
      <Footer />
    </>
  );
};
