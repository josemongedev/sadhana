import ShoppingCartOutlined from "@mui/icons-material/ShoppingCartOutlined";
import { Avatar } from "@mui/material";
import Badge from "@mui/material/Badge";
import { loadCartDispatcher } from "state/async/cart.async";
import { getOrdersDispatcher } from "state/async/orders.async";
import { logoutDispatcher } from "state/async/user.async";
import { selectCart } from "state/selectors/cart.selectors";
import {
  selectCurrentUser,
  selectLoggedUserId,
} from "state/selectors/user.selectors";
import useCheckUserStatus from "hooks/useCheckUserStatus";
import Link from "next/link";
import React, { MouseEventHandler, ReactElement, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as NS from "./Navbar.styles";

interface NavbarProps {}

const Navbar = ({}: NavbarProps): ReactElement => {
  const { quantity } = useSelector(selectCart);
  const currentUser = useSelector(selectCurrentUser);
  const userId = useSelector(selectLoggedUserId);
  const dispatch = useDispatch();
  const { logged } = useCheckUserStatus();

  const handleLogout: MouseEventHandler<HTMLAnchorElement> = (e) => {
    logoutDispatcher(dispatch);
  };

  useEffect(() => {
    if (logged && userId)
      Promise.all([
        (loadCartDispatcher(dispatch, userId),
        getOrdersDispatcher(userId, dispatch)),
      ]);
  }, [dispatch, userId, logged]);
  return (
    <NS.Container>
      <NS.Wrapper>
        <NS.Left>
          <NS.Language>EN</NS.Language>

          {/* <NS.SearchContainer>
            <NS.Input type="text" placeholder="Search" />
            <SearchIcon color="action" fontSize="small" />
          </NS.SearchContainer> */}
        </NS.Left>
        <NS.Center>
          <Link href="/">
            <NS.LogoWrapper>SADHANA</NS.LogoWrapper>
          </Link>
        </NS.Center>
        <NS.Right>
          {!currentUser ? (
            <>
              <NS.MenuItem>
                <Link href="/register">REGISTER</Link>
              </NS.MenuItem>
              <NS.MenuItem>
                <Link href="/login">SIGN IN</Link>
              </NS.MenuItem>
            </>
          ) : (
            <>
              {currentUser && (
                <>
                  <NS.MenuItem>
                    <Link href="/cart">
                        <Badge badgeContent={quantity} color="primary">
                          <ShoppingCartOutlined />
                        </Badge>
                    </Link>
                  </NS.MenuItem>
                </>
              )}
              <NS.MenuItem>
                <Link href="/account">
                    <Avatar
                      color="primary"
                      alt={currentUser.fullname}
                      src={currentUser.img}
                    />
                </Link>
              </NS.MenuItem>
              <NS.MenuItem>
                <NS.AlignCenter>
                  <NS.Anchor onClick={handleLogout}>SIGN OUT</NS.Anchor>
                </NS.AlignCenter>
              </NS.MenuItem>
            </>
          )}
        </NS.Right>
      </NS.Wrapper>
    </NS.Container>
  );
};

export default Navbar;
