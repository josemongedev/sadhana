import styled from "styled-components";
import { mobile } from "responsive";

export const LogoWrapper = styled.h1`
  font-family: "Cinzel Decorative", cursive;
  text-align: center;
  font-weight: bold;
  cursor: pointer;
  ${mobile({ fontSize: "20px" })};
`;

export const Container = styled.div`
  height: 60px;
  ${mobile({ height: "50px" })}
`;

export const Wrapper = styled.div`
  height: 100%;
  padding: 10px 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${mobile({ padding: "10px 0" })};
`;

export const Left = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
`;

export const Language = styled.span`
  margin-left: 10px;
  font-size: 14px;
  cursor: pointer;
`;

export const SearchContainer = styled.div`
  border: 0.5px solid lightgray;
  display: flex;
  align-items: center;
  margin: 25px;
  padding: 5px;
  ${mobile({ margin: "15px" })}
`;

export const Input = styled.input`
  border: none;
  ${mobile({ width: "50px" })}
`;

export const Center = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Logo = styled.h1`
  text-align: center;
  font-weight: bold;
  ${mobile({ fontSize: "20px" })};
`;

export const Right = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  ${mobile({ flex: 1, justifyContent: "center" })}
`;

export const MenuItem = styled.div`
  font-size: 14px;
  cursor: pointer;
  margin-left: 25px;
  ${mobile({ fontSize: "12px", marginLeft: "6px" })}
`;

export const AlignCenter = styled.span`
  justify-content: center;
  align-items: center;
`;

export const Anchor = styled.a``;
