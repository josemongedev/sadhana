import styled from "styled-components";

export const FlexCenter = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Container = styled(FlexCenter)`
  width: 100vw;
  height: 100vh;
`;
export const ImageLayout = styled.img`
  height: 100%;
  width: 50%;
  object-fit: cover;
  object-position: 65%;
`;

export const Column = styled(FlexCenter)`
  margin: 0 20px;
  flex-direction: column;
  width: 100vw;
`;

export const Title = styled.h1`
  font-size: 48px;
  font-weight: 300;
  text-align: center;
`;

export const Anchor = styled.span`
  font-size: 18px;
  cursor: pointer;
  padding: 20px;
  color: white;
  background-color: teal;
`;
