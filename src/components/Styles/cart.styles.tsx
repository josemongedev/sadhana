import { mobile } from "responsive";
import styled from "styled-components";
export const Flex = styled.div`
  display: flex;
`;

export const Container = styled(Flex)`
  min-height: 90vh;
  width: 100%;
  background: linear-gradient(
      rgba(255, 255, 255, 0.5),
      rgba(255, 255, 255, 0.5)
    ),
    url("https://images.pexels.com/photos/5485119/pexels-photo-5485119.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
      center;
  background-size: cover;
  align-items: center;
  justify-content: center;
  padding: 20px;
`;

export const Wrapper = styled.div`
  width: 70vw;
  padding: 20px;
  background-color: white;
  ${mobile({ padding: "10px" })};
`;

export const Title = styled.h1`
  font-weight: 300;
  text-align: center;
`;

export const Top = styled(Flex)`
  align-items: center;
  justify-content: space-between;
  padding: 20px;
`;

interface ITopButton {
  filled?: boolean;
}
export const TopButton = styled.button<ITopButton>`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: ${(props) => props.filled && "none"};
  background-color: ${(props) => (props.filled ? "black" : "transparent")};
  &:disabled {
    background-color: gray;
    cursor: not-allowed;
  }
  color: ${(props) => props.filled && "white"};
`;

export const TopTexts = styled.div`
  ${mobile({ display: "none" })};
`;

export const TopText = styled.span`
  text-decoration: underline;
  cursor: pointer;
  margin: 0 10px;
`;

export const Bottom = styled(Flex)`
  justify-content: space-between;
  ${mobile({ flexDirection: "column" })};
`;

export const Info = styled.div`
  flex: 3;
`;

export const Product = styled(Flex)`
  margin: 20px 0;
  justify-content: space-between;
  ${mobile({ flexDirection: "column" })};
`;

export const ProductDetail = styled(Flex)`
  flex: 2;
`;

export const Image = styled.img`
  width: 200px;
`;

export const Details = styled(Flex)`
  padding: 20px;
  flex-direction: column;
  justify-content: space-around;
`;

export const ProductName = styled.span``;

export const ProductId = styled.span``;

export const ProductColor = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: ${(props) => props.color};
`;

export const ProductSize = styled.span``;

export const PriceDetail = styled(Flex)`
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const ProductAmountContainer = styled(Flex)`
  align-items: center;
  margin-bottom: 20px;
`;

export const ProductAmount = styled.div`
  font-size: 24px;
  margin: 5px;
  ${mobile({ margin: "5px 15px" })};
`;

export const ProductPrice = styled.div`
  font-size: 30px;
  font-weight: 200;
  ${mobile({ marginBottom: "20px" })};
`;

export const ProductOptions = styled.div`
  padding: 20px;
  ${mobile({ padding: "5px " })};
`;

export const HR = styled.hr`
  background-color: #eee;
  border: none;
  height: 1px;
`;

export const Summary = styled.div`
  flex: 1;
  border: 0.5px solid lightgray;
  border-radius: 10px;
  padding: 20px;
  height: 50vh;
`;

export const SummaryTitle = styled.h1`
  font-weight: 200;
`;

interface ISummaryItem {
  total?: boolean;
}
export const SummaryItem = styled(Flex)<ISummaryItem>`
  margin: 30px 0px;
  justify-content: space-between;
  font-weight: ${(props) => props.total && "500"};
  font-size: ${(props) => props.total && "24px"};
`;

export const SummaryItemText = styled.span``;
export const SummaryItemPrice = styled.span``;
export const SummaryButton = styled.button`
  cursor: pointer;
  width: 100%;
  padding: 10px;
  background-color: black;
  color: white;
  font-weight: 600;

  &:disabled {
    background-color: gray;
    cursor: not-allowed;
  }
`;
