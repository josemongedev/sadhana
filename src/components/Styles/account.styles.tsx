import styled from "styled-components";
import { medium, mobile, tablet } from "responsive";

const Flex = styled.div`
  display: flex;
`;

export const Container = styled(Flex)`
  min-height: 90vh;

  width: 100%;
  height: 100%;
  background:
    linear-gradient(rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)),
    url("https://images.pexels.com/photos/1883382/pexels-photo-1883382.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
      center;
  background-size: cover;
  align-items: center;
  justify-content: center;
`;

export const Wrapper = styled.div`
  align-items: center;
  height: auto;
  width: 80%;
  padding: 20px;
  margin: 1%;

  background-color: white;
  ${mobile({ width: "75%", margin: "10%" })};
  ${medium({ width: "80%", margin: "10%" })};
`;

export const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

export const Top = styled(Flex)`
  align-items: center;
  justify-content: space-between;
  padding: 20px;
`;

interface ITopButton {
  filled?: boolean;
}
export const TopButton = styled.button<ITopButton>`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: ${(props) => props.filled && "none"};
  background-color: ${(props) => (props.filled ? "black" : "transparent")};
  color: ${(props) => props.filled && "white"};
`;

export const TopTexts = styled.div`
  ${mobile({ display: "none" })};
`;

export const TopText = styled.span`
  text-decoration: underline;
  cursor: pointer;
  margin: 0 10px;
`;

export const Form = styled(Flex)`
  display: flex;
  flex-direction: column;
`;

export const Row = styled(Flex)`
  flex-direction: row;
  ${mobile({ flexDirection: "column" })};
  ${medium({ flexDirection: "column" })};
  margin: 1% 0;
`;
export const Column = styled(Flex)`
  flex-direction: column;
  height: 50vh;
  margin: 0 2%;
`;

export const Label = styled.label``;
export const FileLabel = styled.label`
  align-self: center;
  white-space: nowrap;
  text-align: center;
  width: 100%;
  margin: 0 1%;
  border: none;
  padding: 15px 20px;
  background-color: #000000;
  color: white;
  cursor: pointer;

  &:disabled {
    color: #727272;
    cursor: not-allowed;
  }
`;

export const Input = styled.input`
  min-width: 40%;
  margin: 10px 0%;
  padding: 10px;
`;

export const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0;
`;

export const LinkInner = styled.span`
  margin: 5px 0;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
  margin-bottom: 10px;
`;

interface IErrorMsg {
  messageColor: string;
}
export const Error = styled.span<IErrorMsg>`
  color: ${(props) => props.messageColor || "red"};
`;
export const Info = styled.span``;

export const File = styled.input`
  margin: 5% 0;
`;

export const Image = styled.img`
  height: 40vh;
  width: 100%;
  object-fit: cover;
`;

export const Legend = styled.span`
  width: 100%;
  font-size: 1rem;
`;

export const Textarea = styled.textarea`
  min-width: 40%;
  margin: 10px 0%;
  padding: 10px;
  resize: none;
`;

export const ImgButton = styled.span`
  margin: 5% 1%;
  border: none;
  padding: 15px 20px;
  background-color: #000000;
  color: white;
  cursor: pointer;

  &:disabled {
    color: #727272;
    cursor: not-allowed;
  }
`;
export const Button = styled.button`
  margin: 5% 1%;
  border: none;
  padding: 15px 20px;
  background-color: #000000;
  color: white;
  cursor: pointer;

  &:disabled {
    color: #727272;
    cursor: not-allowed;
  }
`;
