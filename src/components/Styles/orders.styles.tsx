import { mobile } from "responsive";
import styled from "styled-components";
export const Flex = styled.div`
  display: flex;
`;

export const Container = styled(Flex)`
  min-height: 90vh;

  width: 100%;
  height: 100%;
  background: linear-gradient(
      rgba(255, 255, 255, 0.5),
      rgba(255, 255, 255, 0.5)
    ),
    url("https://images.pexels.com/photos/994517/pexels-photo-994517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940")
      center;
  background-size: cover;
  align-items: center;
  justify-content: center;
`;

export const Wrapper = styled.div`
  width: 80%;
  margin: 2%;
  background-color: white;
  padding: 20px;
  ${mobile({ padding: "10px", width: "100%" })};
`;

export const Title = styled.h1`
  font-weight: 300;
  text-align: center;
`;

export const Top = styled(Flex)`
  align-items: center;
  justify-content: space-between;
  padding: 20px;
`;

interface ITopButton {
  filled?: boolean;
}
export const TopButton = styled.button<ITopButton>`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: ${(props) => props.filled && "none"};
  background-color: ${(props) => (props.filled ? "black" : "transparent")};
  color: ${(props) => props.filled && "white"};
`;

export const TopTexts = styled.div`
  ${mobile({ display: "none" })};
`;

export const TopText = styled.span`
  text-decoration: underline;
  cursor: pointer;
  margin: 0 10px;
`;

export const Bottom = styled(Flex)`
  justify-content: space-between;
  ${mobile({ flexDirection: "column" })};
`;

export const Info = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const Product = styled(Flex)`
  padding: 20px 0;
  justify-content: space-between;
  ${mobile({ flexDirection: "column" })};
`;

export const Details = styled.details`
  border: black solid 1px;
  cursor: pointer;
  display: flex;
  padding: 20px;
  flex-direction: column;
  justify-content: space-around;
`;

export const ProductName = styled.span``;

export const ProductId = styled.span``;

export const ProductColor = styled.span``;

export const ProductSize = styled.span``;

export const ProductAmount = styled.span``;

export const Summary = styled.summary`
  display: flex;
`;
export const SummaryItem = styled.span`
  flex: 1;
`;

export const DetailsExtended = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin: 10px;
`;
