import { mobile } from "responsive";
import styled from "styled-components";

const Flex = styled.div`
  display: flex;
`;

export const Container = styled.div``;

export const Wrapper = styled(Flex)`
  padding: 50px;
  ${mobile({ padding: "10px", flexDirection: "column" })};
`;

export const ImgContainer = styled.div`
  flex: 1;
`;

export const Image = styled.img`
  width: 100%;
  height: 90vh;
  object-fit: contain;
  ${mobile({ height: "40vh" })};
`;

export const InfoContainer = styled.div`
  flex: 1;
  padding: 0 50px;
  ${mobile({ padding: "10px" })};
`;

export const Title = styled.h1`
  font-weight: 200;
`;

export const Desc = styled.p`
  margin: 20px 0;
`;

export const Price = styled.span`
  font-weight: 100;
  font-size: 40px;
`;

export const FilterContainer = styled(Flex)`
  width: 50%;
  margin: 30px 0;
  justify-content: space-between;
  ${mobile({ width: "100%" })};
`;

export const Filter = styled(Flex)`
  align-items: center;
`;

export const FilterTitle = styled.span`
  font-size: 20px;
  font-weight: 200;
`;

interface IFilterColor {
  color: string;
  selected: boolean;
}
export const FilterColor = styled.div<IFilterColor>`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  border-color: ${(props) => props.selected && "red"};
  border-width: ${(props) => props.selected && "thick"};
  background-color: ${(props) => props.color};
  margin: 0 5px;
  cursor: pointer;
`;

export const FilterSize = styled.select`
  margin-left: 10px;
  padding: 5px;
`;

export const FilterSizeOption = styled.option``;

export const AddContainer = styled(Flex)`
  width: 50%;
  align-items: center;
  justify-content: space-between;
  ${mobile({ width: "100%" })};
`;

export const AmountContainer = styled(Flex)`
  align-content: center;
  font-weight: 700;
`;

export const Amount = styled.span`
  width: 30px;
  height: 30px;
  border-radius: 10px;
  border: 1px solid teal;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 5px;
`;

export const Button = styled.button`
  padding: 15px;
  border: 2px solid teal;
  background-color: white;
  cursor: pointer;
  font-weight: 500;

  &:hover {
    background-color: #f8f4f4;
  }
`;
