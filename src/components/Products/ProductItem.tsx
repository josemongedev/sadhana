import { FavoriteBorderOutlined } from "@mui/icons-material";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartOutlined from "@mui/icons-material/ShoppingCartOutlined";
import { TProductMinimal } from "interfaces/types/products";
import Link from "next/link";
import * as PD from "./Product.styles";
import { IProduct } from "interfaces";
import useAddToCart from "../../hooks/useAddToCart";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
interface ProductProps extends TProductMinimal {
  opaque?: boolean;
  product: IProduct;
}

const ProductItem: React.FC<ProductProps> = ({ _id, img, opaque, product }) => {
  const { addToCart } = useAddToCart(product);

  return (
    <PD.Container opacity={opaque ? "0.5" : "1"}>
      <PD.Circle />
      <PD.Image src={img} alt={img} />
      <PD.Info>
        <PD.Icon onClick={addToCart}>
          <AddShoppingCartIcon />
        </PD.Icon>
        <PD.Icon>
          <Link href={`/product/${_id}`}>
            <SearchIcon />
          </Link>
        </PD.Icon>
        {/* <PD.Icon>
          <FavoriteBorderOutlined />
        </PD.Icon> */}
      </PD.Info>
    </PD.Container>
  );
};

export default ProductItem;
