import { IProduct } from "interfaces";
import React, { useEffect, useState } from "react";
import { getProducts } from "services";
import styled from "styled-components";
import ProductItem from "./ProductItem";

const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

interface ProductsProps {
  opaque?: boolean;
  cat?: string | undefined;
  filters?: object;
  sort?: string;
}

const Products: React.FC<ProductsProps> = ({ opaque, cat, filters, sort }) => {
  const [products, setProducts] = useState<IProduct[]>([]);
  const [filteredProducts, setFilteredProducts] = useState<IProduct[]>([]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const res = await getProducts(cat);
        setProducts(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchProducts();
  }, [cat]);

  useEffect(() => {
    cat &&
      setFilteredProducts(
        products.filter((item) =>
          Object.entries(filters as object).every(([key, value]) =>
            (item[key] as string).includes(value)
          )
        )
      );
  }, [cat, filters, products]);

  useEffect(() => {
    if (sort === "newest") {
      setFilteredProducts((prev) =>
        [...prev].sort(
          (a, b) => +new Date(a.createdAt) - +new Date(b.createdAt)
        )
      );
    } else if (sort === "asc") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => Number(a.price) - Number(b.price))
      );
    } else {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => Number(b.price) - Number(a.price))
      );
    }
  }, [sort]);

  return (
    <Container>
      {cat
        ? filteredProducts.map((item) => (
            <ProductItem
              product={item}
              opaque={opaque}
              _id={item._id}
              img={item.img}
              key={item._id}
            />
          ))
        : products
            .slice(0, 8)
            .map((item) => (
              <ProductItem
                product={item}
                opaque={opaque}
                _id={item._id}
                img={item.img}
                key={item._id}
              />
            ))}
    </Container>
  );
};

export default Products;
