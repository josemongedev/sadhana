import { selectUserStatus } from "state/selectors/user.selectors";
import { useRouter } from "next/router";
import React, { PropsWithChildren, useEffect } from "react";
import { useSelector } from "react-redux";

const StaticAuth: React.FC<PropsWithChildren> = ({ children }) => {
  const router = useRouter();
  const userStatus = useSelector(selectUserStatus);
  useEffect(() => {
    if (userStatus !== "loggedIn") {
      router.push("/login");
    }
  }, [userStatus, router]);

  return <>{children}</>;
};

export default StaticAuth;
