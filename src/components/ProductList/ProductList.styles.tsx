import { mobile } from "responsive";
import styled from "styled-components";

const Flex = styled.div`
  display: flex;
`;

export const Container = styled.div``;

export const Title = styled.h1`
  margin: 20px;
`;

export const FilterContainer = styled(Flex)`
  justify-content: space-between;
  ${mobile({ flexDirection: "column" })};
`;

export const Filter = styled.div`
  margin: 20px;
  ${mobile({ width: "0px 10px" })};
`;

export const FilterText = styled.span`
  font-size: 20px;
  font-weight: 600;
  margin-right: 20px;
  ${mobile({ marginRight: "0px" })};
`;

export const Select = styled.select`
  padding: 10px;
  margin-right: 20px;
  ${mobile({ margin: "10px 5px" })};
`;

export const Option = styled.option``;

export const Button = styled.button`
  padding: 10px;
  font-size: 16px;
  background-color: transparent;
  cursor: pointer;
`;
