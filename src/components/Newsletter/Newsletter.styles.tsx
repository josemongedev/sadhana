import styled from "styled-components";
import { mobile } from "responsive";

export const Container = styled.div`
  margin-bottom: 30px;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
`;

export const Title = styled.h1`
  font-size: 60px;
  margin-bottom: 20px;
`;

export const Desc = styled.div`
  margin-bottom: 20px;
  display: flex;
  justify-content: left;
  text-align: left;
  ${mobile({ textAlign: "center" })};
`;

export const InputContainer = styled.div`
  width: 100%;
  height: 40px;
  background-color: white;
  display: flex;
  justify-content: space-between;
  border: 1px solid lightgray;
  ${mobile({ width: "80%" })};
`;

export const Input = styled.input`
  width: 100%;
  border: none;
  padding-left: 20px;
`;

export const Button = styled.button`
  flex: 1;
  border: none;
  background-color: #292a2b;
  color: white;
`;
