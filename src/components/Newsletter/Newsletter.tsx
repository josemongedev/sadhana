import React from "react";
import * as NS from "./Newsletter.styles";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
interface Props {}

const Newsletter = (props: Props) => {
  return (
    <NS.Container>
      <NS.Desc>
        Interested in receiving offers or updates in our catalog?
      </NS.Desc>
      <NS.InputContainer>
        <NS.Input placeholder="Your email" />
        <NS.Button>
          <PlayArrowIcon />
        </NS.Button>
      </NS.InputContainer>
    </NS.Container>
  );
};

export default Newsletter;
