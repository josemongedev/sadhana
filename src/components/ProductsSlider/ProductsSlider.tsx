import CircularSlider from "components/CircularSlider/CircularSlider";
import { Slide } from "components/CircularSlider/CircularSlider.styles";
import ProductItem from "components/Products/ProductItem";
import { IProduct } from "interfaces";
import React, { useEffect, useState } from "react";
import { mobile } from "responsive";
import { getProducts } from "services/products.requests";
import styled from "styled-components";

interface ISection {}
export const Section = styled.section<ISection>`
  height: 100%;
  display: flex;
  flex-direction: column;
  padding: 20px;
  justify-content: space-between;
  ${mobile({ padding: "0px" })}
`;
interface IHeading {}
export const Heading = styled.h1<IHeading>``;

interface ProductsSliderProps {
  cat?: string;
}
const ProductsSlider: React.FC<ProductsSliderProps> = ({ cat }) => {
  const [productSlides, setSlides] = useState<JSX.Element[]>([]);
  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const res = await getProducts(cat);
        const newSlides = res.data.map((item: IProduct) => (
          <Slide key={item._id} $bg="white">
            <ProductItem product={item} opaque img={item.img} _id={item._id} />
          </Slide>
        ));
        setSlides(newSlides);
      } catch (error) {
        console.log(error);
      }
    };
    fetchProducts();
  }, [cat]);
  return <CircularSlider heading="New Arrivals" slides={productSlides} />;
};

export default ProductsSlider;
