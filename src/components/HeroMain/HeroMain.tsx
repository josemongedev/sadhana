import React from "react";
import * as HM from "./HeroMain.styles";

const imgSrc1 =
  "https://images.pexels.com/photos/428338/pexels-photo-428338.jpeg?cs=srgb&dl=pexels-spencer-selover-428338.jpg&fm=jpg";
const imgSrc2 =
  "https://images.pexels.com/photos/1126993/pexels-photo-1126993.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
interface HeroMainProps {}
const HeroMain: React.FC<HeroMainProps> = () => {
  return (
    <HM.Container>
      <HM.Wrapper>
        <HM.Heading>SADHANA</HM.Heading>
        <HM.Heading>FASHION</HM.Heading>
      </HM.Wrapper>
      <HM.Im1 src={imgSrc1} alt="Sadhana Shop Banner" />
      <HM.Im2 src={imgSrc2} alt="Sadhana Shop Banner" />
    </HM.Container>
  );
};

export default HeroMain;
