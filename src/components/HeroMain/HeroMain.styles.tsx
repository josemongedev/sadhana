import { mobile, tablet } from "responsive";
import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  height: 85vh;
  width: 100%;
  margin-bottom: 5vh;
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  position: absolute;
  top: 0;
  left: 0;
`;

export const Im1 = styled(Image)``;

export const Im2 = styled(Image)`
  animation: fadeout 6s infinite alternate;
  @keyframes fadeout {
    0% {
      opacity: 1;
    }
    35% {
      opacity: 1;
    }
    65% {
      opacity: 0;
    }
    100% {
      opacity: 0;
    }
  }
`;

export const Wrapper = styled.div`
  height: 100%;
  padding: 0 20px;
  width: 100%;
  color: black;
  position: absolute;
  z-index: 2;
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
`;

export const Heading = styled.h1`
  font-size: 8vw;
  animation: darkening 6s infinite alternate;
  @keyframes darkening {
    0% {
      color: white;
    }

    100% {
      color: black;
    }
  }
`;
