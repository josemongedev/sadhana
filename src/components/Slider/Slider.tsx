import { ArrowLeft, ArrowRight } from "@mui/icons-material";
import { PropsWithChildren, useState } from "react";
import * as SS from "./Slider.styles";

interface ISliderProps {
  sizeSlides: number;
}
const Slider: React.FC<PropsWithChildren<ISliderProps>> = ({
  sizeSlides,
  children,
}) => {
  const [slideIndex, setSlideIndex] = useState(0);
  const handleClick = (direction: SS.ArrowDirectionType) => {
    if (direction === "left") {
      setSlideIndex(slideIndex > 0 ? slideIndex - 1 : sizeSlides - 1);
    } else {
      setSlideIndex(slideIndex < sizeSlides - 1 ? slideIndex + 1 : 0);
    }
  };
  return (
    <SS.Container>
      <SS.Arrow
        $direction="left"
        onClick={() => handleClick(SS.ArrowDirection.LEFT)}
      >
        <ArrowLeft />
      </SS.Arrow>
      <SS.Wrapper $slideindex={slideIndex}>{children}</SS.Wrapper>
      <SS.Arrow
        $direction="right"
        onClick={() => handleClick(SS.ArrowDirection.RIGHT)}
      >
        <ArrowRight />
      </SS.Arrow>
    </SS.Container>
  );
};

export default Slider;
