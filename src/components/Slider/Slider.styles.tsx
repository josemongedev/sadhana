import styled from "styled-components";
import { mobile } from "responsive";

export const enum ArrowDirection {
  LEFT = "left",
  RIGHT = "right",
}
export type ArrowDirectionType = ArrowDirection.LEFT | ArrowDirection.RIGHT;

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: space-between;
  position: relative;
  overflow: hidden;
  ${mobile({ display: "none" })};
`;

interface IArrow {
  $direction: string;
}
export const Arrow = styled.div<IArrow>`
  width: 50px;
  height: 50px;
  background-color: #fff7f7;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto;
  left: ${(props) =>
    props.$direction === ArrowDirection.LEFT ? "10px" : "auto"};
  right: ${(props) =>
    props.$direction === ArrowDirection.RIGHT ? "10px" : "auto"};
  cursor: pointer;
  opacity: 0.5;
  z-index: 2;
`;
// left: ${(props) => (props.$left ? "10px" : "auto")};
// right: ${(props) => (!props.$left ? "10px" : "auto")};

interface IWrapper {
  $slideindex: number;
}
export const Wrapper = styled.div<IWrapper>`
  height: 100%;
  display: flex;
  transition: all 1.5s ease;
  transform: translateX(${(props) => props.$slideindex * -100}vw);
`;

interface ISlide {
  $bg: string;
}
export const Slide = styled.div<ISlide>`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  /* background-color: #${(props) => props.$bg}; */
`;

export const ImgContainer = styled.div`
  height: 100%;
  flex: 1;
`;

export const Image = styled.img`
  object-fit: contain;
  width: 100%;
  height: 100%;
`;

export const InfoContainer = styled.div`
  flex: 1;
  padding: 50px;
`;

export const Title = styled.h1`
  font-size: 70px;
`;

export const Desc = styled.p`
  margin: 50px 0;
  font-size: 20px;
  font-weight: 500;
  letter-spacing: 3px;
`;

export const Button = styled.button`
  padding: 10px;
  font-size: 20px;
  background-color: transparent;
  cursor: pointer;
`;
