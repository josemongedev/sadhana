import React from "react";

type Props = {};

const Loading = (props: Props) => {
  return (
    <div>
      <span>Loading...</span>
    </div>
  );
};

export default Loading;
