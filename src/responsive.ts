import { css } from "styled-components";

export const mobile = (props: any) => {
  return css`
    @media only screen and (max-width: 522px) {
      ${props}
    }
  `;
};

export const tablet = (props: any) => {
  return css`
    @media only screen and (max-width: 770px) {
      ${props}
    }
  `;
};

export const medium = (props: any) => {
  return css`
    @media only screen and (max-width: 1030px) {
      ${props}
    }
  `;
};

export const laptop = (props: any) => {
  return css`
    @media only screen and (max-width: 1300px) {
      ${props}
    }
  `;
};
