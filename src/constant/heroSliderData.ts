export interface CollectionItem {
  id: number;
  img: string;
  title: string;
  desc: string;
  bg: string;
}
export const sliderItems: CollectionItem[] = [
  {
    id: 1,
    img: "https://images.pexels.com/photos/1381553/pexels-photo-1381553.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "WINTER COLLECTION",
    desc: "40% OFF WHILE SUPPLIES LAST!",
    bg: "fcedfb",
  },
  {
    id: 2,
    img: "https://images.pexels.com/photos/1043474/pexels-photo-1043474.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "SUMMER COLLECTION",
    desc: "DAZZLE EVERYONE WITH THE NEW UPCOMING SUMMER STYLE",
    bg: "f2f9ff",
  },
  {
    id: 3,
    img: "https://images.pexels.com/photos/10481317/pexels-photo-10481317.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "LOUNGEWEAR LOVE",
    desc: "FEELING HOMELY? WHY NOT ALSO WITH COZY WEAR?",
    bg: "f4e6dc",
  },
];
