interface CategoryItem {
  id: number;
  img: string;
  title: string;
  cat: string;
}
export const categories: CategoryItem[] = [
  {
    id: 1,
    img: "https://images.pexels.com/photos/9032473/pexels-photo-9032473.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "FOR WOMEN",
    cat: "women",
  },
  {
    id: 2,
    img: "https://images.pexels.com/photos/6747302/pexels-photo-6747302.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "LOUNGEWEAR LOVE",
    cat: "tshirt",
  },
  {
    id: 3,
    img: "https://images.pexels.com/photos/11050403/pexels-photo-11050403.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "LIGHT JACKETS",
    cat: "jacket",
  },
  {
    id: 4,
    img: "https://images.pexels.com/photos/11050580/pexels-photo-11050580.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "WINTER COATS",
    cat: "coat",
  },
  {
    id: 5,
    img: "https://images.pexels.com/photos/1138560/pexels-photo-1138560.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "LIVELY JEANS",
    cat: "jeans",
  },
  {
    id: 6,
    img: "https://images.pexels.com/photos/1159670/pexels-photo-1159670.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "QUALITY SHOES",
    cat: "shoes",
  },
];
