export const usefulLinks = [
  {
    name: "Home",
    url: ".",
  },
  {
    name: "Cart",
    url: ".",
  },
  {
    name: "Man Fashion",
    url: ".",
  },
  {
    name: "Woman Fashion",
    url: ".",
  },
  {
    name: "Accesories",
    url: ".",
  },
  {
    name: "My Account",
    url: ".",
  },
  {
    name: "Order Tracking",
    url: ".",
  },
  {
    name: "Wishlist",
    url: ".",
  },
  {
    name: "Terms",
    url: ".",
  },
];
