export enum EColors {
  white = "white",
  brown = "brown",
  black = "black",
  red = "red",
  yellow = "yellow",
  blue = "blue",
  green = "green",
  purple = "purple",
}

export enum ESizes {
  XS = "XS",
  S = "S",
  M = "M",
  L = "L",
  XL = "XL",
}
