import { TColors, TSizes } from "./types/products";

interface IFilters {
  color?: TColors;
  size?: TSizes;
}

interface IProduct {
  [key: string]: string | string[] | boolean | number;
  categories: string[];
  color: string[];
  createdAt: string;
  desc: string;
  img: string;
  inStock: boolean;
  price: number;
  size: string[];
  title: string;
  updatedAt: string;
  __v: number;
  _id: string;
}
