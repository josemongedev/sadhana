export interface IUserData {
  username: string;
  fullname: string;
  address: string;
  phone: string;
  email: string;
  password: string;
  isAdmin: boolean;
  active: boolean;
  img: string;
}

export interface IUser extends IUserData {
  _id: string;
  createdAt: string;
}
