import { EColors, ESizes } from "constants";

type TColors = keyof typeof EColors;

type TSizes = keyof typeof ESizes;

type TSort = "newest" | "asc" | "desc";

type TProductMinimal = Pick<IProduct, "img" | "_id">;
