interface ICartItem {
  productId: string;
  quantity: number;
  color: string;
  size: string;
}

type IOrderItem = ICartItem;

interface ICart {
  userId: string;
  products: ICartItem[];
}
