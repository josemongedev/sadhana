import { IUser, IUser } from "interfaces";

type TCheckStatus = "neverLogged" | "loggedIn" | "loggedOut";

type IUserPayload = IUser;
type TDeleteUserPayload = { _id: string };
type TUpdateUserPayload = Partial<IUser>;
interface IUserCheckPayload {
  userStatus: TCheckStatus;
}

interface IUserState {
  currentUser: IUser | null;
  isFetching: boolean;
  error: boolean;
  userStatus: TCheckStatus;
}
