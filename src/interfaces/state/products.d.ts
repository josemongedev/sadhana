export interface IProductsState {
  products: IProduct[];
  isFetching: boolean;
  error: boolean;
}
