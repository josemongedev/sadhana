import { Item } from "@/interfaces";

export interface IOrder {
  _id: string;
  userId: string;
  products: IOrderItem[];
  amount: number;
  address: string;
  status: string;
  createdAt: any;
}

export interface IIncome {
  _id: number;
  total: number;
}

export interface IOrderState {
  orders: IOrder[];
  isFetching: boolean;
  error: boolean;
}
