import { IProduct } from "interfaces";

interface IProductsMap {
  [idx: string]: IProduct;
}

interface ICartState {
  cartId: string | null;
  productsData: IProductsMap;
  cartItems: ICartItem[];
  quantity: number;
  total: number;
  error: boolean;
  isFetching: boolean;
}
