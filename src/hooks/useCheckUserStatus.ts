import { selectLoggedUserId } from "state/selectors/user.selectors";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { getCheckUserStatus } from "services";

// Used AFTER login or registration to determine when to load the cart and orders objects, among other things
const useCheckUserStatus = () => {
  const [logged, setLogged] = useState<boolean>(false);
  const userId = useSelector(selectLoggedUserId) as string;

  useEffect(() => {
    const checkLogStatus = async () => {
      const {
        data: { logged },
      } = await getCheckUserStatus();
      setLogged(logged);
      console.log(logged);
    };
    !logged && userId && checkLogStatus();
  }, [logged, userId]);

  return { logged };
};

export default useCheckUserStatus;
