import {
  updateCartDispatcher,
  createCartDispatcher,
} from "state/async/cart.async";
import { selectCart } from "state/selectors/cart.selectors";
import {
  selectUserStatus,
  selectLoggedUserId,
} from "state/selectors/user.selectors";
import { IProduct } from "interfaces";
import { useRouter } from "next/router";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const useAddToCart = (product: IProduct) => {
  const router = useRouter();
  const cart = useSelector(selectCart);
  const userStatus = useSelector(selectUserStatus);
  const dispatch = useDispatch();
  const userId = useSelector(selectLoggedUserId) as string;

  // State
  const [newCartItem, setNewCartItem] = useState<ICartItem>({
    productId: product!._id,
    quantity: 1,
    color: product!.color[0],
    size: product!.size[0],
  });

  const addToCart = () => {
    if (userStatus === "loggedIn") {
      if (cart.cartId) {
        const repeated = cart.cartItems.find(
          (cItem) => cItem.productId === newCartItem.productId
        );

        let cartItemsUpdate;
        if (repeated) {
          const updItem = { ...repeated, quantity: repeated!.quantity + 1 };
          cartItemsUpdate = [
            ...cart.cartItems.filter(
              (cItem) => cItem.productId !== newCartItem.productId
            ),
            updItem,
          ];
        } else {
          cartItemsUpdate = cart.cartItems.slice();
          cartItemsUpdate.push(newCartItem);
        }
        updateCartDispatcher(dispatch, cart.cartId, cartItemsUpdate, [product]);
      } else {
        createCartDispatcher(dispatch, userId, newCartItem, product);
      }
    } else {
      router.push("/login");
    }
  };

  return { newCartItem, setNewCartItem, addToCart };
};

export default useAddToCart;
