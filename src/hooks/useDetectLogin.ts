import { selectUser } from "state/selectors/user.selectors";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useSelector } from "react-redux";

// Used for register and login pages once the registration or login submit is done
const useDetectLogin = () => {
  const router = useRouter();

  const { isFetching, error, currentUser } = useSelector(selectUser);
  useEffect(() => {
    if (currentUser) router.push("/");
  }, [currentUser, router]);

  return { isFetching, error, currentUser };
};

export default useDetectLogin;
