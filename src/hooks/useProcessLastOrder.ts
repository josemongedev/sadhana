import { IProduct, IOrder } from "interfaces";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProductsList } from "services";
import { deleteCartDispatcher } from "state/async/cart.async";
import { getOrdersDispatcher } from "state/async/orders.async";
import { selectCart } from "state/selectors/cart.selectors";
import { selectOrdersState } from "state/selectors/orders.selectors";
import { selectLoggedUserId } from "state/selectors/user.selectors";

const useProcessLastOrder = (cartId: string) => {
  const router = useRouter();
  // if (!cartId) router.push("/");
  const dispatch = useDispatch();
  const userId = useSelector(selectLoggedUserId) as string;
  const cartCache = useSelector(selectCart);
  const { isFetching, error, orders } = useSelector(selectOrdersState);
  const [validCartId, setValidCartId] = useState<string | null>(null);
  const [productsList, setProductsList] = useState<IProduct[]>([]);
  const [lastOrder, setLastOrder] = useState<IOrder | null>(null);

  //  STEP 1: Confirm cartId query is the current cart ready to be deleted
  // Cart has to be deleted once order is fulfilled(the client does it
  // so the cache is updated), and the cart Id from the order(server)
  // has to coincide with the cart ID from the client's cache.
  // If this doesn't happen, there will be a handled error with Redux,
  // and the server won't do anything.
  useEffect(() => {
    if (cartCache.cartId === cartId) {
      setValidCartId(cartCache.cartId);
      deleteCartDispatcher(dispatch, cartId);
      // This will prevent the redirection once the cart is deleted and the above hook
      // is updated, so that the user can see the page. If the 'old cart ID' isn't set,
      // then it means the cart ID is invalid and page must be redirected.
    } else if (!validCartId) router.push("/");
  }, [cartCache, validCartId, cartId, dispatch, router]);

  // STEP 2: Get orders and get the last order processed
  // This will reload the orders cached with Redux
  useEffect(() => {
    validCartId && getOrdersDispatcher(userId, dispatch);
  }, [userId, dispatch, validCartId]);
  useEffect(() => {
    validCartId && setLastOrder(orders[orders.length - 1]);
  }, [orders, validCartId]);

  // STEP 3: Get the products that are in the order
  useEffect(() => {
    const fetchOrderProducts = async () => {
      if (!error && !isFetching) {
        const idList = orders[orders.length - 1].products.map(
          (product) => product.productId
        );
        const { data: fetchedProducts } = await getProductsList(idList);
        setProductsList(fetchedProducts);
      }
    };
    !isFetching && !error && validCartId && fetchOrderProducts();
  }, [error, isFetching, orders, validCartId]);
  // console.log("cartCache :>> ", cartCache);
  // console.log("cartId :>> ", cartId);
  // console.log("oldCartId :>> ", oldCartId);
  // console.log("lastOrder :>> ", lastOrder);
  // console.log("error :>> ", error);
  return { error, isFetching, lastOrder, validCartId, productsList };
};

export default useProcessLastOrder;
