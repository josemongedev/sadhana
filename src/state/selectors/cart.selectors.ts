import { RootState } from "state";

export const selectCart = (state: RootState) => state.cartState;
export const selectCartId = (state: RootState) => state.cartState.cartId;
