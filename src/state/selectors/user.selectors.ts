import { RootState } from "state";

export const selectUser = (state: RootState) => state.userState;
export const selectCurrentUser = (state: RootState) =>
  state.userState.currentUser;
export const selectUserStatus = (state: RootState) =>
  state.userState.userStatus;
export const selectLoggedUserId = (state: RootState) =>
  state.userState.currentUser?._id;
