import { RootState } from "state";

export const selectOrdersState = (state: RootState) => state.ordersState;
export const selectOrders = (state: RootState) => state.ordersState.orders;
export const selectRecentOrders = (state: RootState) =>
  state.ordersState.orders.slice().reverse();
export const selectOrderById = (orderId: string) => (state: RootState) =>
  state.ordersState.orders.find((order) => orderId === order._id);
