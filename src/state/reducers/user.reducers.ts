import { PayloadAction } from "@reduxjs/toolkit";
import { initialUserState } from "state/slices/user.slice";
import {
  IUserState,
  IUserPayload,
  IUserCheckPayload,
  TDeleteUserPayload,
  TUpdateUserPayload,
  IUser,
} from "interfaces";

// LOGIN Reducers
export const loginStartReducer = (state: IUserState) => {
  state.error = false;
  state.isFetching = true;
};

export const loginSuccessReducer = (
  state: IUserState,
  action: PayloadAction<IUserPayload>
) => {
  state.currentUser = action.payload;
  state.userStatus = "loggedIn";
  state.isFetching = false;
  state.error = false;
};

export const loginFailureReducer = (state: IUserState) => {
  state.isFetching = false;
  state.error = true;
};

// LOAD(get) user Reducers
export const loadUserStartReducer = (state: IUserState) => {
  state.isFetching = true;
  state.error = false;
};

export const loadUserSuccessReducer = (
  state: IUserState,
  action: PayloadAction<IUserPayload>
) => {
  state.currentUser = action.payload;
  state.userStatus = "loggedIn";
  state.isFetching = false;
  state.error = false;
};

export const loadUserFailureReducer = (state: IUserState) => {
  state.isFetching = false;
  state.error = true;
};

// LOGOUT Reducers
export const logoutStartReducer = (state: IUserState) => {
  state.isFetching = true;
  state.error = false;
};

export const logoutSuccessReducer = (state: IUserState) => {
  state.userStatus = "loggedOut";
  state.currentUser = null;
  state.isFetching = false;
  state.error = false;
};

export const logoutFailureReducer = (state: IUserState) => {
  state.isFetching = false;
  state.error = true;
};

// CHECK-USER Reducers

export const checkUserStatusStartReducer = (state: IUserState) => {
  state.error = false;
  state.isFetching = true;
};

export const checkUserStatusSuccessReducer = (
  state: IUserState,
  action: PayloadAction<IUserCheckPayload>
) => {
  state.userStatus = action.payload.userStatus;
  state.isFetching = false;
  state.error = false;
};

export const checkUserStatusFailureReducer = (state: IUserState) => {
  state.isFetching = false;
  state.error = true;
};
// UPDATE Reducers
export const updateUserStartReducer = (state: IUserState) => {
  state.isFetching = true;
  state.error = false;
};

export const updateUserSuccessReducer = (
  state: IUserState,
  action: PayloadAction<TUpdateUserPayload>
) => {
  state.currentUser = {
    ...state.currentUser,
    ...action.payload,
  } as IUser | null;
  state.isFetching = false;
  state.error = false;
};

export const updateUserFailureReducer = (state: IUserState) => {
  state.isFetching = false;
  state.error = true;
};

// DELETE Reducers
export const deleteUserStartReducer = (state: IUserState) => {
  state.isFetching = true;
  state.error = false;
};

export const deleteUserSuccessReducer = (
  state: IUserState,
  action: PayloadAction<TDeleteUserPayload>
) => {
  Object.assign(state, initialUserState);
};

export const deleteUserFailureReducer = (state: IUserState) => {
  state.isFetching = false;
  state.error = true;
};

// Create Reducers
export const registerUserStartReducer = (state: IUserState) => {
  state.isFetching = true;
  state.error = false;
};

export const registerUserSuccessReducer = (
  state: IUserState,
  action: PayloadAction<IUser>
) => {
  state.currentUser = action.payload;
  state.userStatus = "loggedIn";
  state.isFetching = false;
  state.error = false;
};

export const registerUserFailureReducer = (state: IUserState) => {
  state.isFetching = false;
  state.error = true;
};
