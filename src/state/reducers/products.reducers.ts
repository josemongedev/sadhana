import { PayloadAction } from "@reduxjs/toolkit";
import { IProduct, IProductsState } from "interfaces";

// GET Product
export const getProductsStartReducer = (state: IProductsState) => {
  state.isFetching = true;
  state.error = false;
};

export const getProductsSuccessReducer = (
  state: IProductsState,
  action: PayloadAction<IProduct[]>
) => {
  state.products = action.payload;
  state.isFetching = false;
  state.error = false;
};

export const getProductsFailureReducer = (state: IProductsState) => {
  state.isFetching = false;
  state.error = true;
};
