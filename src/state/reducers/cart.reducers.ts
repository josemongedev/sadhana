import { PayloadAction } from "@reduxjs/toolkit";
import { initialCartState } from "state/slices/cart.slice";
import { ICartState, IProduct } from "interfaces";

// LOAD(get) Cart Reducers
export const loadCartStartReducer = (state: ICartState) => {
  state.isFetching = true;
  state.error = false;
};

export const loadCartSuccessReducer = (
  state: ICartState,
  action: PayloadAction<{
    cartId: string;
    cartItems: (ICartItem & { _id: string })[];
    productsData: IProduct[];
  }>
) => {
  const { cartItems, productsData, cartId } = action.payload;
  if (cartItems.length > 0) {
    state.cartId = cartId;
    state.quantity = cartItems.length;
    state.cartItems = cartItems.map((rawItem) => {
      const { _id, ...item } = rawItem;
      return item;
    });
    action.payload.cartItems.forEach(
      (item) =>
        (state.productsData[item.productId] = productsData.find(
          (product) => product._id === item.productId
        ) as IProduct)
    );
    state.total = state.cartItems.reduce(
      (total, item) =>
        total + state.productsData[item.productId]!.price * item.quantity,
      0
    );
  }
  state.isFetching = false;
  state.error = false;
};

export const loadCartFailureReducer = (state: ICartState) => {
  state.isFetching = false;
  state.error = true;
};

// CREATE(post) Cart Reducers
export const createCartStartReducer = (state: ICartState) => {
  state.isFetching = true;
  state.error = false;
};

export const createCartSuccessReducer = (
  state: ICartState,
  action: PayloadAction<{
    cartId: string;
    item: ICartItem;
    productData: IProduct;
  }>
) => {
  const { cartId, item, productData } = action.payload;
  state.cartId = cartId;
  state.productsData[productData._id] = productData;
  state.quantity = 1;
  state.cartItems.push(item);
  state.total = productData.price * item.quantity;
  state.isFetching = false;
  state.error = false;
};

export const createCartFailureReducer = (state: ICartState) => {
  state.isFetching = false;
  state.error = true;
};

// UPDATE(PUT) Cart Reducers
export const updateCartStartReducer = (state: ICartState) => {
  state.isFetching = true;
  state.error = false;
};

export const updateCartSuccessReducer = (
  state: ICartState,
  action: PayloadAction<{ cartItems: ICartItem[]; newProductsData: IProduct[] }>
) => {
  const { cartItems, newProductsData } = action.payload;
  newProductsData.forEach((product) => {
    state.productsData[product._id] = product;
  });
  state.quantity = cartItems.length;
  state.cartItems = cartItems;
  state.total = cartItems.reduce(
    (total, item) =>
      total + state.productsData[item.productId]!.price * item.quantity,
    0
  );
  state.isFetching = false;
  state.error = false;
};

export const updateCartFailureReducer = (state: ICartState) => {
  state.isFetching = false;
  state.error = true;
};

// DELETE Cart Reducers
export const deleteCartStartReducer = (state: ICartState) => {
  state.isFetching = true;
  state.error = false;
};

export const deleteCartSuccessReducer = (state: ICartState) => {
  Object.assign(state, initialCartState);
};

export const deleteCartFailureReducer = (state: ICartState) => {
  state.isFetching = false;
  state.error = true;
};
