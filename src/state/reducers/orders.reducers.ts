import { PayloadAction } from "@reduxjs/toolkit";
import { IOrderState, IOrder } from "interfaces/state/orders";

// GET Order
export const getOrdersStartReducer = (state: IOrderState) => {
  state.isFetching = true;
  state.error = false;
};

export const getOrdersSuccessReducer = (
  state: IOrderState,
  action: PayloadAction<IOrder[]>
) => {
  state.orders = action.payload;
  state.isFetching = false;
  state.error = false;
};

export const getOrdersFailureReducer = (state: IOrderState) => {
  state.isFetching = false;
  state.error = true;
};
