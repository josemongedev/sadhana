import { IUser } from "interfaces";
import {
  deleteUserById,
  getCheckUserStatus,
  getUserById,
  logoutUser,
  postUserLogin,
  registerUser,
  updateUserById,
} from "services";

import { Dispatch } from "redux";
import { loadCartDispatcher } from "./cart.async";
import {
  loginStart,
  loginSuccess,
  loginFailure,
  registerUserStart,
  registerUserSuccess,
  registerUserFailure,
  loadUserStart,
  loadUserSuccess,
  loadUserFailure,
  checkUserStatusStart,
  checkUserStatusSuccess,
  checkUserStatusFailure,
  logoutStart,
  logoutSuccess,
  logoutFailure,
  deleteUserStart,
  deleteUserSuccess,
  deleteUserFailure,
  updateUserStart,
  updateUserSuccess,
  updateUserFailure,
} from "state/slices/user.slice";
import { getOrdersDispatcher } from "./orders.async";

export const loginDispatcher = async (
  dispatch: Dispatch,
  user: Pick<IUser, "password" | "username">
) => {
  dispatch(loginStart());
  try {
    // After this request, cookie is created
    // and authenticated requests can be done
    const { data: userData, headers } = await postUserLogin(user);
    console.log("headers :>> ", headers);
    loadCartDispatcher(dispatch, userData._id);
    getOrdersDispatcher(userData._id, dispatch);
    // Login is done last to avoid redirection before fecthing all the data
    dispatch(loginSuccess(userData));
  } catch (error) {
    dispatch(loginFailure());
  }
};

export const registerUserDispatcher = async (
  user: Partial<IUser>,
  dispatch: Dispatch
) => {
  dispatch(registerUserStart());
  try {
    const { data: userData } = await registerUser(user);
    dispatch(registerUserSuccess(userData));
  } catch (error) {
    dispatch(registerUserFailure());
  }
};

export const loadUserDispatcher = async (
  dispatch: Dispatch,
  userId: string
) => {
  dispatch(loadUserStart());
  try {
    const { data: userData } = await getUserById(userId);
    dispatch(loadUserSuccess(userData));
  } catch (error) {
    dispatch(loadUserFailure());
  }
};

export const checkUserStatusDispatcher = async (dispatch: Dispatch) => {
  dispatch(checkUserStatusStart());
  try {
    const { data: statusData } = await getCheckUserStatus();
    dispatch(
      checkUserStatusSuccess({
        userStatus: statusData.loggedIn ? "loggedIn" : "loggedOut",
      })
    );
  } catch (error) {
    dispatch(checkUserStatusFailure());
  }
};

export const logoutDispatcher = async (dispatch: Dispatch) => {
  dispatch(logoutStart());
  try {
    await logoutUser();
    dispatch(logoutSuccess());
  } catch (error) {
    dispatch(logoutFailure());
  }
};

export const deleteUserDispatcher = async (dispatch: Dispatch, id: string) => {
  dispatch(deleteUserStart());
  try {
    await deleteUserById(id);
    dispatch(deleteUserSuccess({ _id: id }));
  } catch (error) {
    dispatch(deleteUserFailure());
  }
};

export const updateUserDispatcher = async (
  dispatch: Dispatch,
  userId: string,
  user: Partial<IUser>
) => {
  dispatch(updateUserStart());
  try {
    const { data: userData } = await updateUserById(userId, user);
    dispatch(updateUserSuccess(userData));
  } catch (error) {
    dispatch(updateUserFailure());
  }
};
