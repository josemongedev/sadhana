import {
  getProductsStart,
  getProductsSuccess,
  getProductsFailure,
} from "state/slices/products.slice";
import { Dispatch } from "redux";
import { getProducts } from "services";

export const getProductsDispatcher = async (
  userId: string,
  dispatch: Dispatch
) => {
  dispatch(getProductsStart());
  try {
    const { data: ProductData } = await getProducts();
    dispatch(getProductsSuccess(ProductData));
  } catch (error) {
    dispatch(getProductsFailure());
  }
};
