import { IProduct } from "interfaces";
import {
  deleteCart,
  getCartByUserId,
  getProducts,
  postProductToCart,
  putCartWithProducts,
} from "services";
import { Dispatch } from "redux";
import {
  loadCartStart,
  loadCartSuccess,
  loadCartFailure,
  createCartStart,
  createCartSuccess,
  createCartFailure,
  updateCartStart,
  updateCartSuccess,
  updateCartFailure,
  deleteCartStart,
  deleteCartSuccess,
  deleteCartFailure,
} from "state/slices/cart.slice";

export const loadCartDispatcher = async (
  dispatch: Dispatch,
  userId: string
) => {
  dispatch(loadCartStart());
  try {
    const { data: cartGet } = await getCartByUserId(userId);
    const { data: products } = await getProducts();
    if (cartGet?._id)
      dispatch(
        loadCartSuccess({
          cartId: cartGet._id,
          productsData: products,
          cartItems: cartGet.products,
        })
      );
    else throw "Cart not found";
  } catch (error) {
    dispatch(loadCartFailure());
  }
};
export const createCartDispatcher = async (
  dispatch: Dispatch,
  userId: string,
  item: ICartItem,
  productData: IProduct
) => {
  dispatch(createCartStart());
  try {
    const { data: cartPost } = await postProductToCart(item, userId);
    dispatch(createCartSuccess({ cartId: cartPost._id, item, productData }));
  } catch (error) {
    dispatch(createCartFailure());
  }
};

export const updateCartDispatcher = async (
  dispatch: Dispatch,
  cartId: string,
  updatedCartItems: Partial<ICartItem>[],
  newProductsData: IProduct[]
) => {
  dispatch(updateCartStart());

  try {
    const { data: cartPut } = await putCartWithProducts(
      updatedCartItems,
      cartId
    );
    dispatch(
      updateCartSuccess({ cartItems: cartPut.products, newProductsData })
    );
  } catch (error) {
    dispatch(updateCartFailure());
  }
};

export const deleteCartDispatcher = async (
  dispatch: Dispatch,
  cartId: string
) => {
  dispatch(deleteCartStart());
  try {
    await deleteCart(cartId);
    dispatch(deleteCartSuccess());
  } catch (error) {
    dispatch(deleteCartFailure());
  }
};
