import { getOrdersByUserId } from "services";
import { Dispatch } from "redux";
import {
  getOrdersStart,
  getOrdersSuccess,
  getOrdersFailure,
} from "state/slices/orders.slice";

export const getOrdersDispatcher = async (
  userId: string,
  dispatch: Dispatch
) => {
  dispatch(getOrdersStart());
  try {
    const { data: OrderData } = await getOrdersByUserId(userId);
    dispatch(getOrdersSuccess(OrderData));
  } catch (error) {
    dispatch(getOrdersFailure());
  }
};
