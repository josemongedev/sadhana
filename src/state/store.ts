import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { useMemo } from "react";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistReducer,
  PURGE,
  REGISTER,
  REHYDRATE,
} from "redux-persist";
import createWebStorage from "redux-persist/lib/storage/createWebStorage";
import { cartSlice, initialCartState } from "./slices/cart.slice";
import { initialOrdersState, ordersSlice } from "./slices/orders.slice";
import { initialProductsState, ProductsSlice } from "./slices/products.slice";
import { initialUserState, userSlice } from "./slices/user.slice";

// Redux using typescript in Next.js example
// https://github.com/DeMonix666/react-test-nextjs-github
// Added this instead of importing storage, because of this console error:
// redux-persist failed to create sync storage. falling back to noop storage.
// This log occurs on the server-side (when you import storage from redux-persist/lib/storage) because you cannot create the local storage in Node.js. Here is this workaround:
// https://github.com/vercel/next.js/discussions/15687
const createNoopStorage = () => {
  return {
    getItem(_key: any) {
      return Promise.resolve(null);
    },
    setItem(_key: any, value: any) {
      return Promise.resolve(value);
    },
    removeItem(_key: any) {
      return Promise.resolve();
    },
  };
};

const storage =
  typeof window !== "undefined"
    ? createWebStorage("local")
    : createNoopStorage();

export default storage;
// End of "storage" hack

const rootPersistConfig = {
  key: "root",
  version: 1,
  storage,
  whitelist: ["cartState", "userState", "ordersState"],
};
/* COMBINE REDUCERS */
const rootReducer = combineReducers({
  userState: userSlice.reducer,
  cartState: cartSlice.reducer,
  ordersState: ordersSlice.reducer,
  productsState: ProductsSlice.reducer,
});

const persistedReducer = persistReducer(rootPersistConfig, rootReducer);

const appInitialState = {
  userState: initialUserState,
  cartState: initialCartState,
  ordersState: initialOrdersState,
  productsState: initialProductsState,
};

// Explanation on Redux with Next.js and SSR:
// https://www.quintessential.gr/blog/development/how-to-integrate-redux-with-next-js-and-ssr
// Using redux-persist with Next.js. taken from:
// https://github.com/vercel/next.js/blob/canary/examples/with-redux-persist/store.js
function makeStore(initialState = appInitialState) {
  return configureStore({
    preloadedState: initialState,
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
        },
      }),
  });
}

type TStoreRedux = ReturnType<typeof makeStore>;
let store: TStoreRedux | undefined;

export const initializeStore = (preloadedState: any) => {
  let _store = store ?? makeStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = makeStore({
      ...store.getState(),
      ...preloadedState,
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === "undefined") return _store;
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export function useStore(initialState: any) {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
}
// End of SSR/SSG Vercel's hack

// export type  AppStore = ReturnType<typeof make
export type RootState = ReturnType<TStoreRedux["getState"]>;
