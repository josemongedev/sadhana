import { createSlice } from "@reduxjs/toolkit";
import { ICartState } from "interfaces";
import { IProduct } from "interfaces";
import {
  loadCartStartReducer,
  loadCartSuccessReducer,
  loadCartFailureReducer,
  createCartStartReducer,
  createCartSuccessReducer,
  createCartFailureReducer,
  updateCartStartReducer,
  updateCartSuccessReducer,
  updateCartFailureReducer,
  deleteCartStartReducer,
  deleteCartSuccessReducer,
} from "state/reducers/cart.reducers";

export const initialCartState: ICartState = {
  cartId: null,
  productsData: {},
  cartItems: [],
  quantity: 0,
  total: 0,
  error: false,
  isFetching: false,
};

export const cartSlice = createSlice({
  name: "cartState",
  initialState: initialCartState,
  reducers: {
    loadCartStart: loadCartStartReducer,
    loadCartSuccess: loadCartSuccessReducer,
    loadCartFailure: loadCartFailureReducer,
    createCartStart: createCartStartReducer,
    createCartSuccess: createCartSuccessReducer,
    createCartFailure: createCartFailureReducer,
    updateCartStart: updateCartStartReducer,
    updateCartSuccess: updateCartSuccessReducer,
    updateCartFailure: updateCartFailureReducer,
    deleteCartStart: deleteCartStartReducer,
    deleteCartSuccess: deleteCartSuccessReducer,
    deleteCartFailure: createCartFailureReducer,
  },
});

export const {
  loadCartStart,
  loadCartSuccess,
  loadCartFailure,
  createCartStart,
  createCartSuccess,
  createCartFailure,
  updateCartStart,
  updateCartSuccess,
  updateCartFailure,
  deleteCartStart,
  deleteCartSuccess,
  deleteCartFailure,
} = cartSlice.actions;
// export const { reducer: cartReducer } = cartSlice;
