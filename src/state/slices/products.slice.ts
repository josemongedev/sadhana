import { createSlice } from "@reduxjs/toolkit";
import { IProductsState } from "interfaces";
import {
  getProductsStartReducer,
  getProductsSuccessReducer,
  getProductsFailureReducer,
} from "state/reducers/products.reducers";

export const initialProductsState: IProductsState = {
  products: [],
  isFetching: false,
  error: false,
};

export const ProductsSlice = createSlice({
  name: "ProductsState",
  initialState: initialProductsState,
  reducers: {
    getProductsStart: getProductsStartReducer,
    getProductsSuccess: getProductsSuccessReducer,
    getProductsFailure: getProductsFailureReducer,
  },
});

export const { getProductsStart, getProductsSuccess, getProductsFailure } =
  ProductsSlice.actions;
// export const { reducer: ProductReducer } = ProductSlice;
