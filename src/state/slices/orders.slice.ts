import { createSlice } from "@reduxjs/toolkit";
import { IOrderState } from "interfaces";
import {
  getOrdersStartReducer,
  getOrdersSuccessReducer,
  getOrdersFailureReducer,
} from "state/reducers/orders.reducers";

export const initialOrdersState: IOrderState = {
  orders: [],
  isFetching: false,
  error: false,
};

export const ordersSlice = createSlice({
  name: "ordersState",
  initialState: initialOrdersState,
  reducers: {
    getOrdersStart: getOrdersStartReducer,
    getOrdersSuccess: getOrdersSuccessReducer,
    getOrdersFailure: getOrdersFailureReducer,
  },
});

export const { getOrdersStart, getOrdersSuccess, getOrdersFailure } =
  ordersSlice.actions;
// export const { reducer: OrderReducer } = OrderSlice;
