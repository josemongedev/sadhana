/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["images.pexels.com", "fakestoreapi.com"],
  },
  env: {
    SADHANA_API_BACKEND: process.env.SADHANA_API_BACKEND,
    FRONTEND_URL: process.env.FRONTEND_URL,
  },
};
